--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- Code belongs to TI3 DT
-- Stimuli Generator for ISE resp. XC2C256-CPLD related timing simulation just an example
--
-- VCS: git@BitBucket.org/schaefers/DT-DEMO-SG4CPLD.git
-- PUB: https://pub.informatik.haw-hamburg.de/home/pub/prof/schaefers_michael/*_DT/_CODE_/...
--
-- HISTORY:
-- ========
-- WS13/14
--      release for TI3 DT WS13/14  by Michael Schaefers
-- WS14/15 (141104):
--      update for TI3 DT WS14/15  by Michael Schaefers 
------------------------------------------------------------------------------
-- BEMERKUNG:
-- ==========
-- Deutscher Kommentar fuer Erklaerungen fuer speziell Studenten/VHDL-Anfaenger - dieser Kommentar wuerde im "Normalfall" fehlen
-- Englischer Kommentar als "normaler" Kommentar



library work;
    use work.all;

library std;
    use std.textio.all;                 -- benoetigt vom "markStart"-PROCESS

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;       -- benoetigt fuer conv_std_logic_vector() (und Entscheidung gegen "numeric_std_unsigned")
    use ieee.std_logic_unsigned.all;    -- benoetigt fuer conv_std_logic_vector() (und Entscheidung gegen "numeric_std_unsigned")



-- sg ::= Stimuli Generator    
entity sg is
    generic (
        msbPos : natural := 11
    );
    port (
        x         : out std_logic_vector(msbPos downto 0);
        req       : out std_logic;
        clk       : out std_logic;
        nRes      : out std_logic
    );--]port
    constant one : std_logic_vector(msbPos downto 0) := "000000000001";
end entity sg;



architecture beh of sg is
    
    -- Das Arbeiten mit 1/8 Takt-Zyklen ist eigentlich "uebertrieben",
    --   aber es erleichtert das Lesen der Waves
    --   und es bleibt auf den Stimuli Generator beschraenkt
    -- 8*5ns = 40ns Takt-Periode => 25MHz Takt-Frequenz
    constant oneEigthClockCycle     : time  := 5 ns;
    constant clkPeriod              : time  := 40 ns;
    
    signal   nres_s                 : std_logic  := 'L';    -- "internal" nres - workaround to enable reading nres without using "buffer-ports"
    signal   clk_s                  : std_logic;            -- "internal" clk - workaround to enable reading clk without using "buffer-ports"
    
    signal   simulationRunning_s    : boolean  := true;     -- for internal signalling that simulation is running
    
begin

    resGen:                                                 -- RESet GENerator
    process is
    begin
        nres_s <= '0';                                      -- set low active reset
        for i in 1 to 2 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        -- since 2 rising clk edges have passed, synchronous reset must have been executed
        
        wait for oneEigthClockCycle;
        nres_s <= '1';                                      -- clear high active reset
        -- 1/8 clk period after rising clk edge reset is vanishing
        
        wait;
    end process resGen;
    --
    nRes <= nres_s;
    
    clkgen:
        process is
        begin
            while simulationRunning_s loop
                clk_s <= '0';
                wait for 20 ns;
                clk_s <= '1';
                wait for 20 ns;
            end loop; -- unnötig(2), aber u.U. besser lesbar
        end process clkgen;
    --
    clk <= clk_s;
    
    sg:                                                     -- Stimuli Generator
    process is
        variable x_v : std_logic_vector(msbPos downto 0);
        variable req_v: std_logic;
    begin
        
        if  nres_s/='0'  then  wait until nres_s='0';  end if;
        x_v := (others => '0');
        x_v(msbPos) := '1';
        req_v := '0';            -- just set any defined data
        if  nres_s/='1'  then  wait until nres_s='1';  end if;
        --reset has passed
        
        -- give CPLD some time to wake up
        -- for  i in 1 to 10 loop
            -- wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        -- end loop;
        -- CPLD is configured and able to react

        req <= req_v;
        for i in 1 to 4096 loop
            x <= x_v;
            req_v := '1';
            req <= req_v;
            for i in 1 to 14 loop
                wait until '1' = clk_s and clk_s'event;
            end loop;
            x_v := x_v + one;
        end loop;
        
        wait for 3*clkPeriod;
        req <= '0';
        
---------------------------0.25-------------------------------------        
        
        -- x_v := (others => '0');
        -- x_v(msbPos-2) := '1';
        -- x <= x_v;
        -- req_v := '1';
        -- req <= req_v;
        -- wait for 14*clkPeriod;
        -- req_v := '0';
        -- req <= req_v;
        -- wait for clkPeriod;
-----------------------------------------------------------------------        

----------------------------0.75---------------------------------------

        -- x_v := (others => '0');
        -- x_v(msbPos-1) := '1';
        -- x_v(msbPos-2) := '1';
        -- x <= x_v;
        -- req_v := '1';
        -- req <= req_v;
        -- wait for 14*clkPeriod;
        -- req_v := '0';
        -- req <= req_v;
        -- wait for clkPeriod;

-----------------------------------------------------------------------

----------------------------0.5625-------------------------------------        

        -- x_v := (others => '0');
        -- x_v(msbPos-1) := '1';
        -- x_v(msbPos-4) := '1';
        -- req_v := '1';
        -- x <= x_v;
        -- req_v := '1';
        -- req <= req_v;
        -- wait for 14*clkPeriod;
        -- req_v := '0';
        -- req <= req_v;
        -- wait for clkPeriod;
-----------------------------------------------------------------------
----------------------------(-0.75)-------------------------------------
        -- available <= '0';
        -- x_v := (others => '0');
        -- x_v(msbPos) := '1';
        -- x_v(msbPos-2) := '1';
        -- req_v := '1';
        -- req <= req_v;
        -- x <= x_v;
        -- for i in 1 to 14 loop
            -- wait until '1'=clk_s and clk_s'event;
            -- available <= '0';
        -- end loop;
        -- available <= '1';
        -- req_v := '0';
        -- req <= req_v;
        -- wait for clkPeriod;
        
        ----------------------------(-0.996582...)-------------------------------------        

        -- x_v := (others => '0');
        -- x_v(msbPos) := '1';
        -- x_v(msbPos-9) := '1';
        -- x_v(msbPos-10) := '1';
        -- x_v(msbPos-11) := '1';
        -- req_v := '1';
        -- x <= x_v;
        -- req_v := '1';
        -- req <= req_v;
        -- wait for 14*clkPeriod;
        -- req_v := '0';
        -- req <= req_v;
        -- wait for clkPeriod;
-----------------------------------------------------------------------
        
        -- stop SG after 10 clk cycles - assuming computed data is stable afterwards
        for  i in 1 to 10 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        simulationRunning_s <= false;                       -- stop clk generation
        --
        wait;
    end process sg;
end architecture beh;