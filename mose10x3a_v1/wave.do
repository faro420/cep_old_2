onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 40 Clock
add wave -noupdate /tbdut/clk_s
add wave -noupdate -divider -height 40 Reset
add wave -noupdate /tbdut/nRes_s
add wave -noupdate -divider -height 40 Ready
add wave -noupdate /tbdut/rdy_s
add wave -noupdate -divider -height 20 Request
add wave -noupdate /tbdut/req_s
add wave -noupdate -divider -height 40 Argument
add wave -noupdate /tbdut/x_s
add wave -noupdate -divider -height 20 <NULL>
add wave -noupdate /tbdut/available_s
add wave -noupdate -divider -height 40 {Result f(x)}
add wave -noupdate /tbdut/fx_s
add wave -noupdate /tbdut/fx
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {597 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {86 ns} {1242 ns}
