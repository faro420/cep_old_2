--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity dut is
    generic (
        bitWidth : natural := 12                            -- bit width - determines computation accuracy
    );
    port (
        fx   : out std_logic_vector( bitWidth-1 downto 0 );  -- f(x)/result/"square root" coded in Q0.<bitWidth-1>
        rdy  : out std_logic;                                -- Ready/result of computation valid
        --
        x    : in std_logic_vector( bitWidth-1 downto 0 );   -- argument coded in Q0.<bitWidth-1>
        req  : in std_logic;                                 -- REQuest for computation/argument valid
        clk  : in std_logic;                                 -- Clock(25MHz for your FPGA)
        nRes : in std_logic                                  -- Reset, low active
    );
    --
    constant msbPos   : natural := bitWidth-1;                 -- Most Significant Bit Position
    constant three    : natural := 3;
    constant one      : std_logic_vector (three downto 0) := "0001";
    constant zero     : std_logic_vector (msbPos downto 0) := (others => '0');
    constant negOne   : std_logic_vector (msbPos downto 0) := (msbPos => '1', others => '0');
    constant thirteen : std_logic_vector (three downto 0) := "1101";
end entity dut;

architecture beh of dut is

    type index_table_t is array ( natural range <> ) of natural;
    constant index : index_table_t(0 to msbPos) := (
        (11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0)
    );

    signal x_cs:std_logic_vector(msbPos downto 0):= (others =>'0');
    signal x_ns:std_logic_vector(msbPos downto 0);
	
    signal fx_cs:std_logic_vector(msbPos downto 0):= (others =>'0');
    signal fx_ns:std_logic_vector(msbPos downto 0);	

    signal rdy_cs: std_logic :='1';
    signal rdy_ns: std_logic ;

    signal approx_cs:std_logic_vector(msbPos downto 0) := (others =>'0');
    signal approx_ns:std_logic_vector(msbPos downto 0);
    
    signal counter_cs : std_logic_vector(three downto 0) := thirteen;
    signal counter_ns : std_logic_vector(three downto 0);
    
    signal corrNeg_cs  : std_logic := '0';
    signal corrNeg_ns  : std_logic;
begin   
calc_sqr_root:
    process ( x_cs, req, counter_cs, rdy_cs, x ) is
        variable x_v      : std_logic_vector(msbPos downto 0);
        variable fx_v     : std_logic_vector(msbPos downto 0);
        variable approx_v : std_logic_vector(msbPos downto 0);
        variable req_v    : std_logic;   
        variable rdy_v    : std_logic;
        variable calcPath_v  : std_logic;
        variable corrNeg_v   : std_logic; 
        variable n_v         : natural;        
        variable counter_v   : std_logic_vector(three  downto 0);
        variable tempR_v     : std_logic_vector(msbPos downto 0);
        variable tempS_v     : std_logic_vector(msbPos downto 0);
        variable index_v     : natural;
        subtype sub is std_logic_vector( 11 downto 0 );
    begin
        x_v := x_cs;
        fx_v:= fx_cs;
        rdy_v := rdy_cs;
        approx_v := approx_cs;
        counter_v := counter_cs;
        n_v := to_integer(unsigned(counter_v));
        index_v := 0;
        corrNeg_v := corrNeg_cs;
        tempR_v := (others=>'0');
        tempS_v := (others=>'0');
        req_v:=req;
        
    if (rdy_v ='1') and (req_v ='1') and (n_v >= msbPos+2) then
        counter_v := (others => '0');
        n_v := 0;
        approx_v := (others => '0');
        x_v:= x;
        rdy_v:='0';
        calcPath_v := '0';
        case sub'(x_v) is
            when zero   => approx_v := zero;
            when negOne => approx_v := negOne; 
            when others => calcPath_v := '1';
        end case;        
    elsif corrNeg_v /= '0' and n_v >= msbPos+2 then
        calcPath_v := '0';
        corrNeg_v := '0';
    elsif corrNeg_v /= '1' and n_v >= msbPos+1 then
        calcPath_v := '0';     
    elsif n_v >= 1 and n_v <= msbPos+1 and x_v /= zero and x_v /= negOne then
        calcPath_v := '1';
    elsif corrNeg_v /= '0' then
        calcPath_v :='1';
    else
        calcPath_v := '0';
    end if;
    
    preparePath:
    if calcPath_v /= '0' then
        if n_v <= msbPos then
            index_v := index(n_v);
            if x_v(msbPos) /= '1' then
                tempS_v := std_logic_vector(unsigned(approx_v) sll 1); -- 2s
                tempS_v(index_v) := '1'; -- 2s + 2^n (msbPos-n_v)
                tempS_v := std_logic_vector(unsigned(tempS_v) srl n_v); -- 2^n*(...)
            else
                tempS_v := x_v;
                x_v := (others => '0');
                corrNeg_v := '1';
            end if;
        else
            tempS_v := approx_v;
            x_v := (others => '0');
        end if;
    end if preparePath;
    
    actualCalcPath:
    if calcPath_v /= '0' then
        tempR_v := std_logic_vector(unsigned(x_v) - unsigned(tempS_v)); -- r' = r - ...
        if corrNeg_v /= '0' and n_v <= 0 then
            x_v := tempR_v;
        elsif corrNeg_v /= '0' and n_v >= msbPos+1 then
            approx_v := tempR_v;
        else
            if tempR_v(msbPos) /= '1' then
                approx_v(index_v) := '1';
                x_v := tempR_v;
            end if;
        end if;
    end if actualCalcPath;
    
    if rdy_v /= '1' then
        if n_v >= msbPos+2 then
            rdy_v := '1';
            fx_v  := approx_v;
        else
            counter_v := std_logic_vector(unsigned(counter_v) + unsigned(one));
        end if;
    end if;
    
    fx_ns      <= fx_v;
    approx_ns  <= approx_v;
    corrNeg_ns <= corrNeg_v;
    x_ns       <= x_v;
    rdy_ns     <= rdy_v;
    counter_ns <= counter_v;
    end process calc_sqr_root;
    
    
    reg:
    process ( clk ) is
    begin
        if clk = '1' and clk'event then
            if nres = '0' then
                x_cs       <= (others => '0');
				fx_cs      <= (others => '0');
                rdy_cs     <= '1';
                approx_cs  <= (others => '0');
                counter_cs <= thirteen;
                corrNeg_cs <= '0';
            else
                x_cs       <= x_ns;
                fx_cs      <= fx_ns;
                rdy_cs     <= rdy_ns;
                approx_cs  <= approx_ns;
                counter_cs <= counter_ns;
                corrNeg_cs <= corrNeg_ns;
            end if;
        end if;
    end process reg;
    rdy <= rdy_cs;
    fx <= fx_cs;
end architecture beh;