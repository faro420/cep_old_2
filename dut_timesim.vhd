--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: dut_timesim.vhd
-- /___/   /\     Timestamp: Thu Apr 28 11:54:32 2016
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -s 3 -pcf dut.pcf -rpw 100 -tpw 0 -ar Structure -tm dut -insert_pp_buffers true -w -dir netgen/par -ofmt vhdl -sim dut.ncd dut_timesim.vhd 
-- Device	: 6slx16csg324-3 (PRODUCTION 1.23 2013-10-13)
-- Input file	: dut.ncd
-- Output file	: E:\CE\Praktikum\cep2_ss2016\ise14x7_v1\iseWRK\netgen\par\dut_timesim.vhd
-- # of Entities	: 1
-- Design Name	: dut
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

-- entity dut is
  -- port (
    -- req : in STD_LOGIC := 'X'; 
    -- clk : in STD_LOGIC := 'X'; 
    -- nRes : in STD_LOGIC := 'X'; 
    -- rdy : out STD_LOGIC; 
    -- x : in STD_LOGIC_VECTOR ( 11 downto 0 ); 
    -- fx : out STD_LOGIC_VECTOR ( 11 downto 0 ) 
  -- );
-- end dut;

architecture Structure of dut is
  signal rdy_cs_PWR_4_o_AND_2_o11 : STD_LOGIC; 
  signal req_IBUF_0 : STD_LOGIC; 
  signal counter_cs_1_2_1778 : STD_LOGIC; 
  signal counter_cs_0_2_1779 : STD_LOGIC; 
  signal counter_cs_3_2_1780 : STD_LOGIC; 
  signal counter_cs_2_2_1781 : STD_LOGIC; 
  signal rdy_cs_1782 : STD_LOGIC; 
  signal x_cs_11_x_11_mux_14_OUT_11_Q : STD_LOGIC; 
  signal x_8_IBUF_0 : STD_LOGIC; 
  signal x_11_IBUF_0 : STD_LOGIC; 
  signal N196_0 : STD_LOGIC; 
  signal counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out6 : STD_LOGIC; 
  signal N192 : STD_LOGIC; 
  signal Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q : STD_LOGIC; 
  signal Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q : STD_LOGIC; 
  signal rdy_cs_2_1794 : STD_LOGIC; 
  signal counter_cs_1_1_1795 : STD_LOGIC; 
  signal counter_cs_0_1_1796 : STD_LOGIC; 
  signal counter_cs_3_1_1797 : STD_LOGIC; 
  signal counter_cs_2_1_1798 : STD_LOGIC; 
  signal rdy_cs_1_1799 : STD_LOGIC; 
  signal Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q : STD_LOGIC; 
  signal Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o : STD_LOGIC; 
  signal N126 : STD_LOGIC; 
  signal corrNeg_cs_PWR_4_o_AND_42_o : STD_LOGIC; 
  signal N32 : STD_LOGIC; 
  signal counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out2 : STD_LOGIC; 
  signal calc_sqr_root_calcPath_v : STD_LOGIC; 
  signal corrNeg_cs_counter_cs_3_AND_41_o : STD_LOGIC; 
  signal N127 : STD_LOGIC; 
  signal N40 : STD_LOGIC; 
  signal calc_sqr_root_rdy_v : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out12 : STD_LOGIC; 
  signal Q_n0179_inv : STD_LOGIC; 
  signal clk_BUFGP : STD_LOGIC; 
  signal nRes_inv : STD_LOGIC; 
  signal x_2_IBUF_0 : STD_LOGIC; 
  signal corrNeg_cs_counter_cs_3_AND_41_o1_1823 : STD_LOGIC; 
  signal GND_4_o_GND_4_o_sub_28_OUT_2_0 : STD_LOGIC; 
  signal GND_4_o_GND_4_o_sub_28_OUT_11_0 : STD_LOGIC; 
  signal N129 : STD_LOGIC; 
  signal corrNeg_cs_counter_cs_3_AND_41_o11 : STD_LOGIC; 
  signal counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out : STD_LOGIC; 
  signal x_0_IBUF_0 : STD_LOGIC; 
  signal x_cs_11_GND_4_o_mux_26_OUT_0_Q : STD_LOGIC; 
  signal Mmux_calc_sqr_root_calcPath_v181 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out13 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out16 : STD_LOGIC; 
  signal N48 : STD_LOGIC; 
  signal N158 : STD_LOGIC; 
  signal N176 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_calcPath_v16_1850 : STD_LOGIC; 
  signal N149 : STD_LOGIC; 
  signal GND_4_o_GND_4_o_sub_28_OUT_0_0 : STD_LOGIC; 
  signal N190 : STD_LOGIC; 
  signal x_9_IBUF_0 : STD_LOGIC; 
  signal x_5_IBUF_0 : STD_LOGIC; 
  signal N198_0 : STD_LOGIC; 
  signal x_7_IBUF_0 : STD_LOGIC; 
  signal x_6_IBUF_0 : STD_LOGIC; 
  signal N194_0 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out14 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out10_0 : STD_LOGIC; 
  signal N44 : STD_LOGIC; 
  signal N150_0 : STD_LOGIC; 
  signal N42 : STD_LOGIC; 
  signal calc_sqr_root_corrNeg_v : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out15_0 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v1412_1878 : STD_LOGIC; 
  signal N116 : STD_LOGIC; 
  signal counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out9 : STD_LOGIC; 
  signal x_1_IBUF_0 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v141 : STD_LOGIC; 
  signal x_10_IBUF_0 : STD_LOGIC; 
  signal N38 : STD_LOGIC; 
  signal N60 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_calcPath_v18_1894 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out7 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out6_0 : STD_LOGIC; 
  signal N117 : STD_LOGIC; 
  signal N213 : STD_LOGIC; 
  signal Sh11 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out22 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out20 : STD_LOGIC; 
  signal Mmux_x_cs_11_x_11_mux_14_OUT161_1903 : STD_LOGIC; 
  signal Mmux_x_cs_11_x_11_mux_14_OUT16 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out9 : STD_LOGIC; 
  signal GND_4_o_GND_4_o_sub_28_OUT_9_0 : STD_LOGIC; 
  signal N119 : STD_LOGIC; 
  signal N188 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_calcPath_v11_1915 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_calcPath_v1 : STD_LOGIC; 
  signal x_4_IBUF_0 : STD_LOGIC; 
  signal N209_0 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out5 : STD_LOGIC; 
  signal N101 : STD_LOGIC; 
  signal N102 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v1101 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v13 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v133_1926 : STD_LOGIC; 
  signal N46 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v162_0 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v1102 : STD_LOGIC; 
  signal x_cs_11_GND_4_o_mux_26_OUT_1_Q : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v163_1931 : STD_LOGIC; 
  signal GND_4_o_GND_4_o_sub_28_OUT_1_0 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v171 : STD_LOGIC; 
  signal x_cs_11_GND_4_o_mux_26_OUT_2_Q : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v172_1935 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_Q_1936 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v181_0 : STD_LOGIC; 
  signal N50 : STD_LOGIC; 
  signal x_cs_11_GND_4_o_mux_26_OUT_3_Q : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v182_1940 : STD_LOGIC; 
  signal GND_4_o_GND_4_o_sub_28_OUT_3_0 : STD_LOGIC; 
  signal GND_4_o_GND_4_o_sub_28_OUT_6_0 : STD_LOGIC; 
  signal N104 : STD_LOGIC; 
  signal N52 : STD_LOGIC; 
  signal N20 : STD_LOGIC; 
  signal x_cs_11_GND_4_o_mux_26_OUT_4_Q : STD_LOGIC; 
  signal GND_4_o_GND_4_o_sub_28_OUT_4_0 : STD_LOGIC; 
  signal N54 : STD_LOGIC; 
  signal N22 : STD_LOGIC; 
  signal x_cs_11_GND_4_o_mux_26_OUT_5_Q : STD_LOGIC; 
  signal GND_4_o_GND_4_o_sub_28_OUT_5_0 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out17 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v1131 : STD_LOGIC; 
  signal N165 : STD_LOGIC; 
  signal N24_0 : STD_LOGIC; 
  signal N164 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_Q_1957 : STD_LOGIC; 
  signal x_cs_11_GND_4_o_mux_26_OUT_6_Q : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out18 : STD_LOGIC; 
  signal N26 : STD_LOGIC; 
  signal N168 : STD_LOGIC; 
  signal N167 : STD_LOGIC; 
  signal GND_4_o_GND_4_o_sub_28_OUT_7_0 : STD_LOGIC; 
  signal x_cs_11_GND_4_o_mux_26_OUT_7_Q : STD_LOGIC; 
  signal Sh8 : STD_LOGIC; 
  signal N56_0 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v1132_0 : STD_LOGIC; 
  signal x_cs_11_GND_4_o_mux_26_OUT_8_Q : STD_LOGIC; 
  signal GND_4_o_GND_4_o_sub_28_OUT_8_0 : STD_LOGIC; 
  signal Sh9 : STD_LOGIC; 
  signal N58_0 : STD_LOGIC; 
  signal x_cs_11_GND_4_o_mux_26_OUT_9_Q : STD_LOGIC; 
  signal Sh10 : STD_LOGIC; 
  signal x_cs_11_GND_4_o_mux_26_OUT_10_Q : STD_LOGIC; 
  signal GND_4_o_GND_4_o_sub_28_OUT_10_0 : STD_LOGIC; 
  signal N200 : STD_LOGIC; 
  signal clk_BUFGP_IBUFG_0 : STD_LOGIC; 
  signal counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out1 : STD_LOGIC; 
  signal counter_cs_3_PWR_4_o_LessThan_16_o : STD_LOGIC; 
  signal counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out10 : STD_LOGIC; 
  signal x_3_IBUF_0 : STD_LOGIC; 
  signal Sh71 : STD_LOGIC; 
  signal Sh91 : STD_LOGIC; 
  signal N201 : STD_LOGIC; 
  signal counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out3 : STD_LOGIC; 
  signal counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out8 : STD_LOGIC; 
  signal N203 : STD_LOGIC; 
  signal counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out4 : STD_LOGIC; 
  signal counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out5 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out2 : STD_LOGIC; 
  signal Sh25_0 : STD_LOGIC; 
  signal Sh51 : STD_LOGIC; 
  signal Sh61_0 : STD_LOGIC; 
  signal Sh110 : STD_LOGIC; 
  signal N217 : STD_LOGIC; 
  signal N218 : STD_LOGIC; 
  signal counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out7 : STD_LOGIC; 
  signal N92 : STD_LOGIC; 
  signal N147 : STD_LOGIC; 
  signal N160_0 : STD_LOGIC; 
  signal Sh31 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o1_2009 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_calcPath_v12_2010 : STD_LOGIC; 
  signal corrNeg_cs_2011 : STD_LOGIC; 
  signal N63 : STD_LOGIC; 
  signal calc_sqr_root_index_v_0_0 : STD_LOGIC; 
  signal GND_4_o_Decoder_30_OUT_4_3_11_0 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_calcPath_v13_2016 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_calcPath_v14_2017 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_calcPath_v15_2018 : STD_LOGIC; 
  signal N106 : STD_LOGIC; 
  signal Sh41 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v131_2021 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_calcPath_v17_2022 : STD_LOGIC; 
  signal GND_4_o_Decoder_30_OUT_10_3_11 : STD_LOGIC; 
  signal N107 : STD_LOGIC; 
  signal N109 : STD_LOGIC; 
  signal N141 : STD_LOGIC; 
  signal N144 : STD_LOGIC; 
  signal N91 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v4 : STD_LOGIC; 
  signal N94 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v5 : STD_LOGIC; 
  signal N162 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out19 : STD_LOGIC; 
  signal N18 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v6 : STD_LOGIC; 
  signal N80 : STD_LOGIC; 
  signal N111 : STD_LOGIC; 
  signal N112 : STD_LOGIC; 
  signal N114 : STD_LOGIC; 
  signal N136 : STD_LOGIC; 
  signal N137 : STD_LOGIC; 
  signal N139 : STD_LOGIC; 
  signal N96 : STD_LOGIC; 
  signal N97 : STD_LOGIC; 
  signal N34 : STD_LOGIC; 
  signal N186 : STD_LOGIC; 
  signal N99 : STD_LOGIC; 
  signal N131 : STD_LOGIC; 
  signal N132 : STD_LOGIC; 
  signal N134 : STD_LOGIC; 
  signal N146 : STD_LOGIC; 
  signal N16 : STD_LOGIC; 
  signal N71 : STD_LOGIC; 
  signal N74 : STD_LOGIC; 
  signal N77 : STD_LOGIC; 
  signal N65 : STD_LOGIC; 
  signal N68 : STD_LOGIC; 
  signal N62 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v132_2059 : STD_LOGIC; 
  signal N93 : STD_LOGIC; 
  signal N98 : STD_LOGIC; 
  signal N103 : STD_LOGIC; 
  signal N108 : STD_LOGIC; 
  signal N143 : STD_LOGIC; 
  signal N142 : STD_LOGIC; 
  signal N138 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out8 : STD_LOGIC; 
  signal N128 : STD_LOGIC; 
  signal N133 : STD_LOGIC; 
  signal N113 : STD_LOGIC; 
  signal N118 : STD_LOGIC; 
  signal N36 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_0_Q_31 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_1_Q_23 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_2_Q_15 : STD_LOGIC; 
  signal ProtoComp31_CYINITVCC_1 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_3_Q_1 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_4_Q_69 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_5_Q_61 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_6_Q_53 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_7_Q_39 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_8_Q_105 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_9_Q_97 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_10_Q_89 : STD_LOGIC; 
  signal Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_11_Q_76 : STD_LOGIC; 
  signal ProtoComp35_IINV_OUT : STD_LOGIC; 
  signal nRes_inv_non_inverted : STD_LOGIC; 
  signal clk_BUFGP_IBUFG_117 : STD_LOGIC; 
  signal x_10_IBUF_120 : STD_LOGIC; 
  signal x_11_IBUF_123 : STD_LOGIC; 
  signal x_0_IBUF_126 : STD_LOGIC; 
  signal x_1_IBUF_129 : STD_LOGIC; 
  signal x_2_IBUF_132 : STD_LOGIC; 
  signal x_3_IBUF_135 : STD_LOGIC; 
  signal x_4_IBUF_138 : STD_LOGIC; 
  signal x_5_IBUF_141 : STD_LOGIC; 
  signal x_6_IBUF_144 : STD_LOGIC; 
  signal x_7_IBUF_149 : STD_LOGIC; 
  signal x_8_IBUF_156 : STD_LOGIC; 
  signal x_9_IBUF_161 : STD_LOGIC; 
  signal req_IBUF_164 : STD_LOGIC; 
  signal N262 : STD_LOGIC; 
  signal N56 : STD_LOGIC; 
  signal N273 : STD_LOGIC; 
  signal N272 : STD_LOGIC; 
  signal Sh25 : STD_LOGIC; 
  signal N275 : STD_LOGIC; 
  signal N274 : STD_LOGIC; 
  signal Sh110_pack_6 : STD_LOGIC; 
  signal N277 : STD_LOGIC; 
  signal N276 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v181 : STD_LOGIC; 
  signal N269 : STD_LOGIC; 
  signal N268 : STD_LOGIC; 
  signal Sh61 : STD_LOGIC; 
  signal Sh51_pack_8 : STD_LOGIC; 
  signal N270 : STD_LOGIC; 
  signal N271 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out15 : STD_LOGIC; 
  signal N24 : STD_LOGIC; 
  signal N258 : STD_LOGIC; 
  signal N259 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out22_pack_4 : STD_LOGIC; 
  signal N267 : STD_LOGIC; 
  signal N266 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v162 : STD_LOGIC; 
  signal N194 : STD_LOGIC; 
  signal N260 : STD_LOGIC; 
  signal N58 : STD_LOGIC; 
  signal N160 : STD_LOGIC; 
  signal GND_4_o_Decoder_30_OUT_4_3_11 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out6 : STD_LOGIC; 
  signal N196 : STD_LOGIC; 
  signal N38_pack_9 : STD_LOGIC; 
  signal N265 : STD_LOGIC; 
  signal Mmux_calc_sqr_root_approx_v1132 : STD_LOGIC; 
  signal N264 : STD_LOGIC; 
  signal N209 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out10 : STD_LOGIC; 
  signal N198 : STD_LOGIC; 
  signal rdy_cs_PWR_4_o_AND_2_o_mmx_out8_pack_5 : STD_LOGIC; 
  signal N150 : STD_LOGIC; 
  signal N36_pack_8 : STD_LOGIC; 
  signal NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_0_Q : STD_LOGIC; 
  signal NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_1_Q : STD_LOGIC; 
  signal NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_2_Q : STD_LOGIC; 
  signal NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_3_Q : STD_LOGIC; 
  signal NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_0_Q : STD_LOGIC; 
  signal NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_1_Q : STD_LOGIC; 
  signal NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_2_Q : STD_LOGIC; 
  signal NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_3_Q : STD_LOGIC; 
  signal NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_0_Q : STD_LOGIC; 
  signal NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_1_Q : STD_LOGIC; 
  signal NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_2_Q : STD_LOGIC; 
  signal NlwBufferSignal_fx_10_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_fx_0_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_fx_11_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_fx_1_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_fx_2_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_fx_3_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_fx_4_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_rdy_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_fx_5_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_fx_6_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_fx_7_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_fx_8_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_fx_9_OBUF_I : STD_LOGIC; 
  signal NlwBufferSignal_clk_BUFGP_BUFG_IN : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_3_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_3_IN : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_2_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_2_IN : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_1_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_1_IN : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_0_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_0_IN : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_2_CLK : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_1_CLK : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_0_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_11_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_10_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_9_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_9_IN : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_8_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_8_IN : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_7_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_7_IN : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_6_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_6_IN : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_5_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_4_CLK : STD_LOGIC; 
  signal NlwBufferSignal_fx_cs_4_IN : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_6_CLK : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_6_IN : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_5_CLK : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_5_IN : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_4_CLK : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_3_CLK : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_8_CLK : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_7_CLK : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_11_CLK : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_11_IN : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_10_CLK : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_10_IN : STD_LOGIC; 
  signal NlwBufferSignal_approx_cs_9_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_3_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_3_IN : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_2_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_1_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_0_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_0_IN : STD_LOGIC; 
  signal NlwBufferSignal_x_cs_5_CLK : STD_LOGIC; 
  signal NlwBufferSignal_x_cs_4_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_2_2_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_2_2_IN : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_2_1_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_2_1_IN : STD_LOGIC; 
  signal NlwBufferSignal_rdy_cs_2_CLK : STD_LOGIC; 
  signal NlwBufferSignal_rdy_cs_2_IN : STD_LOGIC; 
  signal NlwBufferSignal_rdy_cs_1_CLK : STD_LOGIC; 
  signal NlwBufferSignal_rdy_cs_1_IN : STD_LOGIC; 
  signal NlwBufferSignal_x_cs_7_CLK : STD_LOGIC; 
  signal NlwBufferSignal_x_cs_6_CLK : STD_LOGIC; 
  signal NlwBufferSignal_corrNeg_cs_CLK : STD_LOGIC; 
  signal NlwBufferSignal_rdy_cs_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_0_2_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_0_1_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_0_1_IN : STD_LOGIC; 
  signal NlwBufferSignal_x_cs_11_CLK : STD_LOGIC; 
  signal NlwBufferSignal_x_cs_10_CLK : STD_LOGIC; 
  signal NlwBufferSignal_x_cs_1_CLK : STD_LOGIC; 
  signal NlwBufferSignal_x_cs_0_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_1_2_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_1_2_IN : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_1_1_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_1_1_IN : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_3_2_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_3_1_CLK : STD_LOGIC; 
  signal NlwBufferSignal_counter_cs_3_1_IN : STD_LOGIC; 
  signal NlwBufferSignal_x_cs_3_CLK : STD_LOGIC; 
  signal NlwBufferSignal_x_cs_2_CLK : STD_LOGIC; 
  signal NlwBufferSignal_x_cs_9_CLK : STD_LOGIC; 
  signal NlwBufferSignal_x_cs_8_CLK : STD_LOGIC; 
  signal NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_CO_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_CO_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_CO_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_CO_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_CO_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_CO_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_CO_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_CO_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_CO_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_CO_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_N1_2_C6LUT_O_UNCONNECTED : STD_LOGIC; 
  signal GND : STD_LOGIC; 
  signal VCC : STD_LOGIC; 
  signal NLW_N1_C6LUT_O_UNCONNECTED : STD_LOGIC; 
  signal x_cs : STD_LOGIC_VECTOR ( 11 downto 0 ); 
  signal calc_sqr_root_counter_v : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal approx_cs : STD_LOGIC_VECTOR ( 11 downto 0 ); 
  signal fx_cs : STD_LOGIC_VECTOR ( 11 downto 0 ); 
  signal calc_sqr_root_approx_v : STD_LOGIC_VECTOR ( 11 downto 0 ); 
  signal counter_cs : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal calc_sqr_root_index_v : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal GND_4_o_GND_4_o_sub_28_OUT : STD_LOGIC_VECTOR ( 11 downto 0 ); 
  signal calc_sqr_root_x_v : STD_LOGIC_VECTOR ( 11 downto 0 ); 
begin
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => GND_4_o_GND_4_o_sub_28_OUT(3),
      O => GND_4_o_GND_4_o_sub_28_OUT_3_0
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => GND_4_o_GND_4_o_sub_28_OUT(2),
      O => GND_4_o_GND_4_o_sub_28_OUT_2_0
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_BMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => GND_4_o_GND_4_o_sub_28_OUT(1),
      O => GND_4_o_GND_4_o_sub_28_OUT_1_0
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_AMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => GND_4_o_GND_4_o_sub_28_OUT(0),
      O => GND_4_o_GND_4_o_sub_28_OUT_0_0
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_3_Q : X_LUT6
    generic map(
      LOC => "SLICE_X8Y5",
      INIT => X"CCC999C9CC999999"
    )
    port map (
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR2 => Mmux_calc_sqr_root_approx_v181_0,
      ADR0 => N50,
      ADR5 => Mmux_calc_sqr_root_approx_v1102,
      ADR1 => x_cs_11_GND_4_o_mux_26_OUT_3_Q,
      ADR4 => Mmux_calc_sqr_root_approx_v182_1940,
      O => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_3_Q_1
    );
  ProtoComp31_CYINITVCC : X_ONE
    generic map(
      LOC => "SLICE_X8Y5"
    )
    port map (
      O => ProtoComp31_CYINITVCC_1
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_Q : X_CARRY4
    generic map(
      LOC => "SLICE_X8Y5"
    )
    port map (
      CI => '0',
      CYINIT => ProtoComp31_CYINITVCC_1,
      CO(3) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_Q_1936,
      CO(2) => NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_CO_2_UNCONNECTED,
      CO(1) => NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_CO_1_UNCONNECTED,
      CO(0) => NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_CO_0_UNCONNECTED,
      DI(3) => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_3_Q,
      DI(2) => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_2_Q,
      DI(1) => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_1_Q,
      DI(0) => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_0_Q,
      O(3) => GND_4_o_GND_4_o_sub_28_OUT(3),
      O(2) => GND_4_o_GND_4_o_sub_28_OUT(2),
      O(1) => GND_4_o_GND_4_o_sub_28_OUT(1),
      O(0) => GND_4_o_GND_4_o_sub_28_OUT(0),
      S(3) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_3_Q_1,
      S(2) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_2_Q_15,
      S(1) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_1_Q_23,
      S(0) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_0_Q_31
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_2_Q : X_LUT6
    generic map(
      LOC => "SLICE_X8Y5",
      INIT => X"FF00F807FF0008F7"
    )
    port map (
      ADR2 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR0 => Mmux_calc_sqr_root_approx_v171,
      ADR4 => N48,
      ADR1 => Mmux_calc_sqr_root_approx_v1102,
      ADR3 => x_cs_11_GND_4_o_mux_26_OUT_2_Q,
      ADR5 => Mmux_calc_sqr_root_approx_v172_1935,
      O => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_2_Q_15
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_1_Q : X_LUT6
    generic map(
      LOC => "SLICE_X8Y5",
      INIT => X"CCC9CC9999C99999"
    )
    port map (
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR0 => N46,
      ADR4 => Mmux_calc_sqr_root_approx_v162_0,
      ADR2 => Mmux_calc_sqr_root_approx_v1102,
      ADR1 => x_cs_11_GND_4_o_mux_26_OUT_1_Q,
      ADR5 => Mmux_calc_sqr_root_approx_v163_1931,
      O => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_1_Q_23
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_0_Q : X_LUT6
    generic map(
      LOC => "SLICE_X8Y5",
      INIT => X"88878887BBBBBBB4"
    )
    port map (
      ADR5 => N44,
      ADR4 => Mmux_calc_sqr_root_approx_v1101,
      ADR0 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out,
      ADR2 => Mmux_calc_sqr_root_approx_v13,
      ADR1 => Mmux_calc_sqr_root_calcPath_v181,
      ADR3 => Mmux_calc_sqr_root_approx_v133_1926,
      O => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_0_Q_31
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => GND_4_o_GND_4_o_sub_28_OUT(7),
      O => GND_4_o_GND_4_o_sub_28_OUT_7_0
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => GND_4_o_GND_4_o_sub_28_OUT(6),
      O => GND_4_o_GND_4_o_sub_28_OUT_6_0
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_BMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => GND_4_o_GND_4_o_sub_28_OUT(5),
      O => GND_4_o_GND_4_o_sub_28_OUT_5_0
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_AMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => GND_4_o_GND_4_o_sub_28_OUT(4),
      O => GND_4_o_GND_4_o_sub_28_OUT_4_0
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_7_Q : X_LUT6
    generic map(
      LOC => "SLICE_X8Y6",
      INIT => X"EFEF4040CCEFCC40"
    )
    port map (
      ADR5 => rdy_cs_PWR_4_o_AND_2_o_mmx_out18,
      ADR3 => Mmux_calc_sqr_root_approx_v1131,
      ADR2 => Mmux_calc_sqr_root_approx_v1102,
      ADR0 => N26,
      ADR1 => N168,
      ADR4 => N167,
      O => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_7_Q_39
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_Q : X_CARRY4
    generic map(
      LOC => "SLICE_X8Y6"
    )
    port map (
      CI => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_Q_1936,
      CYINIT => '0',
      CO(3) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_Q_1957,
      CO(2) => NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_CO_2_UNCONNECTED,
      CO(1) => NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_CO_1_UNCONNECTED,
      CO(0) => NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_CO_0_UNCONNECTED,
      DI(3) => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_3_Q,
      DI(2) => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_2_Q,
      DI(1) => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_1_Q,
      DI(0) => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_0_Q,
      O(3) => GND_4_o_GND_4_o_sub_28_OUT(7),
      O(2) => GND_4_o_GND_4_o_sub_28_OUT(6),
      O(1) => GND_4_o_GND_4_o_sub_28_OUT(5),
      O(0) => GND_4_o_GND_4_o_sub_28_OUT(4),
      S(3) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_7_Q_39,
      S(2) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_6_Q_53,
      S(1) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_5_Q_61,
      S(0) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_4_Q_69
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_6_Q : X_LUT6
    generic map(
      LOC => "SLICE_X8Y6",
      INIT => X"CEC4FF00CEC4CEC4"
    )
    port map (
      ADR4 => rdy_cs_PWR_4_o_AND_2_o_mmx_out17,
      ADR5 => Mmux_calc_sqr_root_approx_v1131,
      ADR0 => Mmux_calc_sqr_root_approx_v1102,
      ADR3 => N165,
      ADR2 => N24_0,
      ADR1 => N164,
      O => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_6_Q_53
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_5_Q : X_LUT6
    generic map(
      LOC => "SLICE_X8Y6",
      INIT => X"CCCCCCCCC3C93399"
    )
    port map (
      ADR4 => rdy_cs_PWR_4_o_AND_2_o_mmx_out6_0,
      ADR5 => N54,
      ADR0 => Mmux_calc_sqr_root_approx_v1102,
      ADR2 => Mmux_calc_sqr_root_approx_v1101,
      ADR3 => N22,
      ADR1 => x_cs_11_GND_4_o_mux_26_OUT_5_Q,
      O => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_5_Q_61
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_4_Q : X_LUT6
    generic map(
      LOC => "SLICE_X8Y6",
      INIT => X"CCCC99C9CCCC33C3"
    )
    port map (
      ADR5 => rdy_cs_PWR_4_o_AND_2_o_mmx_out5,
      ADR4 => N52,
      ADR2 => Mmux_calc_sqr_root_approx_v1102,
      ADR0 => Mmux_calc_sqr_root_approx_v1101,
      ADR3 => N20,
      ADR1 => x_cs_11_GND_4_o_mux_26_OUT_4_Q,
      O => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_4_Q_69
    );
  GND_4_o_GND_4_o_sub_28_OUT_11_GND_4_o_GND_4_o_sub_28_OUT_11_DMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => GND_4_o_GND_4_o_sub_28_OUT(11),
      O => GND_4_o_GND_4_o_sub_28_OUT_11_0
    );
  GND_4_o_GND_4_o_sub_28_OUT_11_GND_4_o_GND_4_o_sub_28_OUT_11_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => GND_4_o_GND_4_o_sub_28_OUT(10),
      O => GND_4_o_GND_4_o_sub_28_OUT_10_0
    );
  GND_4_o_GND_4_o_sub_28_OUT_11_GND_4_o_GND_4_o_sub_28_OUT_11_BMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => GND_4_o_GND_4_o_sub_28_OUT(9),
      O => GND_4_o_GND_4_o_sub_28_OUT_9_0
    );
  GND_4_o_GND_4_o_sub_28_OUT_11_GND_4_o_GND_4_o_sub_28_OUT_11_AMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => GND_4_o_GND_4_o_sub_28_OUT(8),
      O => GND_4_o_GND_4_o_sub_28_OUT_8_0
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_11_Q : X_LUT6
    generic map(
      LOC => "SLICE_X8Y7",
      INIT => X"00F5330A00A0335F"
    )
    port map (
      ADR2 => x_11_IBUF_0,
      ADR5 => x_cs(11),
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR4 => Mmux_calc_sqr_root_approx_v1101,
      ADR1 => N213,
      ADR3 => calc_sqr_root_calcPath_v,
      O => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_11_Q_76
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_Q : X_CARRY4
    generic map(
      LOC => "SLICE_X8Y7"
    )
    port map (
      CI => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_Q_1957,
      CYINIT => '0',
      CO(3) => NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_CO_3_UNCONNECTED,
      CO(2) => NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_CO_2_UNCONNECTED,
      CO(1) => NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_CO_1_UNCONNECTED,
      CO(0) => NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_CO_0_UNCONNECTED,
      DI(3) => NLW_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_3_UNCONNECTED,
      DI(2) => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_2_Q,
      DI(1) => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_1_Q,
      DI(0) => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_0_Q,
      O(3) => GND_4_o_GND_4_o_sub_28_OUT(11),
      O(2) => GND_4_o_GND_4_o_sub_28_OUT(10),
      O(1) => GND_4_o_GND_4_o_sub_28_OUT(9),
      O(0) => GND_4_o_GND_4_o_sub_28_OUT(8),
      S(3) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_11_Q_76,
      S(2) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_10_Q_89,
      S(1) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_9_Q_97,
      S(0) => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_8_Q_105
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_10_Q : X_LUT6
    generic map(
      LOC => "SLICE_X8Y7",
      INIT => X"CCCCCC93CCCC9393"
    )
    port map (
      ADR5 => Mmux_calc_sqr_root_approx_v141,
      ADR0 => Sh10,
      ADR2 => Mmux_calc_sqr_root_approx_v1132_0,
      ADR3 => Mmux_calc_sqr_root_approx_v1101,
      ADR4 => N60,
      ADR1 => x_cs_11_GND_4_o_mux_26_OUT_10_Q,
      O => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_10_Q_89
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_9_Q : X_LUT6
    generic map(
      LOC => "SLICE_X8Y7",
      INIT => X"F0F0E1C3E1C3E1C3"
    )
    port map (
      ADR5 => rdy_cs_PWR_4_o_AND_2_o_mmx_out10_0,
      ADR0 => Sh9,
      ADR1 => N58_0,
      ADR3 => Mmux_calc_sqr_root_approx_v1132_0,
      ADR4 => Mmux_calc_sqr_root_approx_v1101,
      ADR2 => x_cs_11_GND_4_o_mux_26_OUT_9_Q,
      O => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_9_Q_97
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_8_Q : X_LUT6
    generic map(
      LOC => "SLICE_X8Y7",
      INIT => X"FE01EE11FA05AA55"
    )
    port map (
      ADR2 => rdy_cs_PWR_4_o_AND_2_o_mmx_out9,
      ADR5 => Sh8,
      ADR0 => N56_0,
      ADR1 => Mmux_calc_sqr_root_approx_v1132_0,
      ADR4 => Mmux_calc_sqr_root_approx_v1101,
      ADR3 => x_cs_11_GND_4_o_mux_26_OUT_8_Q,
      O => Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_8_Q_105
    );
  nRes_IBUF : X_BUF
    generic map(
      LOC => "PAD177",
      PATHPULSE => 202 ps
    )
    port map (
      O => nRes_inv_non_inverted,
      I => nRes
    );
  ProtoComp35_IMUX : X_BUF
    generic map(
      LOC => "PAD177",
      PATHPULSE => 202 ps
    )
    port map (
      I => ProtoComp35_IINV_OUT,
      O => nRes_inv
    );
  ProtoComp35_IINV : X_INV
    generic map(
      LOC => "PAD177",
      PATHPULSE => 202 ps
    )
    port map (
      I => nRes_inv_non_inverted,
      O => ProtoComp35_IINV_OUT
    );
  clk_BUFGP_IBUFG : X_BUF
    generic map(
      LOC => "PAD216",
      PATHPULSE => 202 ps
    )
    port map (
      O => clk_BUFGP_IBUFG_117,
      I => clk
    );
  ProtoComp36_IMUX : X_BUF
    generic map(
      LOC => "PAD216",
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP_IBUFG_117,
      O => clk_BUFGP_IBUFG_0
    );
  x_10_IBUF : X_BUF
    generic map(
      LOC => "PAD167",
      PATHPULSE => 202 ps
    )
    port map (
      O => x_10_IBUF_120,
      I => x(10)
    );
  ProtoComp36_IMUX_1 : X_BUF
    generic map(
      LOC => "PAD167",
      PATHPULSE => 202 ps
    )
    port map (
      I => x_10_IBUF_120,
      O => x_10_IBUF_0
    );
  x_11_IBUF : X_BUF
    generic map(
      LOC => "PAD170",
      PATHPULSE => 202 ps
    )
    port map (
      O => x_11_IBUF_123,
      I => x(11)
    );
  ProtoComp36_IMUX_2 : X_BUF
    generic map(
      LOC => "PAD170",
      PATHPULSE => 202 ps
    )
    port map (
      I => x_11_IBUF_123,
      O => x_11_IBUF_0
    );
  x_0_IBUF : X_BUF
    generic map(
      LOC => "PAD163",
      PATHPULSE => 202 ps
    )
    port map (
      O => x_0_IBUF_126,
      I => x(0)
    );
  ProtoComp36_IMUX_3 : X_BUF
    generic map(
      LOC => "PAD163",
      PATHPULSE => 202 ps
    )
    port map (
      I => x_0_IBUF_126,
      O => x_0_IBUF_0
    );
  x_1_IBUF : X_BUF
    generic map(
      LOC => "PAD159",
      PATHPULSE => 202 ps
    )
    port map (
      O => x_1_IBUF_129,
      I => x(1)
    );
  ProtoComp36_IMUX_4 : X_BUF
    generic map(
      LOC => "PAD159",
      PATHPULSE => 202 ps
    )
    port map (
      I => x_1_IBUF_129,
      O => x_1_IBUF_0
    );
  x_2_IBUF : X_BUF
    generic map(
      LOC => "PAD160",
      PATHPULSE => 202 ps
    )
    port map (
      O => x_2_IBUF_132,
      I => x(2)
    );
  ProtoComp36_IMUX_5 : X_BUF
    generic map(
      LOC => "PAD160",
      PATHPULSE => 202 ps
    )
    port map (
      I => x_2_IBUF_132,
      O => x_2_IBUF_0
    );
  x_3_IBUF : X_BUF
    generic map(
      LOC => "PAD161",
      PATHPULSE => 202 ps
    )
    port map (
      O => x_3_IBUF_135,
      I => x(3)
    );
  ProtoComp36_IMUX_6 : X_BUF
    generic map(
      LOC => "PAD161",
      PATHPULSE => 202 ps
    )
    port map (
      I => x_3_IBUF_135,
      O => x_3_IBUF_0
    );
  x_4_IBUF : X_BUF
    generic map(
      LOC => "PAD158",
      PATHPULSE => 202 ps
    )
    port map (
      O => x_4_IBUF_138,
      I => x(4)
    );
  ProtoComp36_IMUX_7 : X_BUF
    generic map(
      LOC => "PAD158",
      PATHPULSE => 202 ps
    )
    port map (
      I => x_4_IBUF_138,
      O => x_4_IBUF_0
    );
  x_5_IBUF : X_BUF
    generic map(
      LOC => "PAD162",
      PATHPULSE => 202 ps
    )
    port map (
      O => x_5_IBUF_141,
      I => x(5)
    );
  ProtoComp36_IMUX_8 : X_BUF
    generic map(
      LOC => "PAD162",
      PATHPULSE => 202 ps
    )
    port map (
      I => x_5_IBUF_141,
      O => x_5_IBUF_0
    );
  x_6_IBUF : X_BUF
    generic map(
      LOC => "PAD164",
      PATHPULSE => 202 ps
    )
    port map (
      O => x_6_IBUF_144,
      I => x(6)
    );
  ProtoComp36_IMUX_9 : X_BUF
    generic map(
      LOC => "PAD164",
      PATHPULSE => 202 ps
    )
    port map (
      I => x_6_IBUF_144,
      O => x_6_IBUF_0
    );
  fx_10_OBUF : X_OBUF
    generic map(
      LOC => "PAD180"
    )
    port map (
      I => NlwBufferSignal_fx_10_OBUF_I,
      O => fx(10)
    );
  x_7_IBUF : X_BUF
    generic map(
      LOC => "PAD171",
      PATHPULSE => 202 ps
    )
    port map (
      O => x_7_IBUF_149,
      I => x(7)
    );
  ProtoComp36_IMUX_10 : X_BUF
    generic map(
      LOC => "PAD171",
      PATHPULSE => 202 ps
    )
    port map (
      I => x_7_IBUF_149,
      O => x_7_IBUF_0
    );
  fx_0_OBUF : X_OBUF
    generic map(
      LOC => "PAD182"
    )
    port map (
      I => NlwBufferSignal_fx_0_OBUF_I,
      O => fx(0)
    );
  fx_11_OBUF : X_OBUF
    generic map(
      LOC => "PAD188"
    )
    port map (
      I => NlwBufferSignal_fx_11_OBUF_I,
      O => fx(11)
    );
  x_8_IBUF : X_BUF
    generic map(
      LOC => "PAD168",
      PATHPULSE => 202 ps
    )
    port map (
      O => x_8_IBUF_156,
      I => x(8)
    );
  ProtoComp36_IMUX_11 : X_BUF
    generic map(
      LOC => "PAD168",
      PATHPULSE => 202 ps
    )
    port map (
      I => x_8_IBUF_156,
      O => x_8_IBUF_0
    );
  fx_1_OBUF : X_OBUF
    generic map(
      LOC => "PAD183"
    )
    port map (
      I => NlwBufferSignal_fx_1_OBUF_I,
      O => fx(1)
    );
  x_9_IBUF : X_BUF
    generic map(
      LOC => "PAD169",
      PATHPULSE => 202 ps
    )
    port map (
      O => x_9_IBUF_161,
      I => x(9)
    );
  ProtoComp36_IMUX_12 : X_BUF
    generic map(
      LOC => "PAD169",
      PATHPULSE => 202 ps
    )
    port map (
      I => x_9_IBUF_161,
      O => x_9_IBUF_0
    );
  req_IBUF : X_BUF
    generic map(
      LOC => "PAD157",
      PATHPULSE => 202 ps
    )
    port map (
      O => req_IBUF_164,
      I => req
    );
  ProtoComp36_IMUX_13 : X_BUF
    generic map(
      LOC => "PAD157",
      PATHPULSE => 202 ps
    )
    port map (
      I => req_IBUF_164,
      O => req_IBUF_0
    );
  fx_2_OBUF : X_OBUF
    generic map(
      LOC => "PAD184"
    )
    port map (
      I => NlwBufferSignal_fx_2_OBUF_I,
      O => fx(2)
    );
  fx_3_OBUF : X_OBUF
    generic map(
      LOC => "PAD181"
    )
    port map (
      I => NlwBufferSignal_fx_3_OBUF_I,
      O => fx(3)
    );
  fx_4_OBUF : X_OBUF
    generic map(
      LOC => "PAD174"
    )
    port map (
      I => NlwBufferSignal_fx_4_OBUF_I,
      O => fx(4)
    );
  rdy_OBUF : X_OBUF
    generic map(
      LOC => "PAD172"
    )
    port map (
      I => NlwBufferSignal_rdy_OBUF_I,
      O => rdy
    );
  fx_5_OBUF : X_OBUF
    generic map(
      LOC => "PAD175"
    )
    port map (
      I => NlwBufferSignal_fx_5_OBUF_I,
      O => fx(5)
    );
  fx_6_OBUF : X_OBUF
    generic map(
      LOC => "PAD176"
    )
    port map (
      I => NlwBufferSignal_fx_6_OBUF_I,
      O => fx(6)
    );
  fx_7_OBUF : X_OBUF
    generic map(
      LOC => "PAD173"
    )
    port map (
      I => NlwBufferSignal_fx_7_OBUF_I,
      O => fx(7)
    );
  fx_8_OBUF : X_OBUF
    generic map(
      LOC => "PAD178"
    )
    port map (
      I => NlwBufferSignal_fx_8_OBUF_I,
      O => fx(8)
    );
  fx_9_OBUF : X_OBUF
    generic map(
      LOC => "PAD179"
    )
    port map (
      I => NlwBufferSignal_fx_9_OBUF_I,
      O => fx(9)
    );
  clk_BUFGP_BUFG : X_CKBUF
    generic map(
      LOC => "BUFGMUX_X3Y13",
      PATHPULSE => 202 ps
    )
    port map (
      I => NlwBufferSignal_clk_BUFGP_BUFG_IN,
      O => clk_BUFGP
    );
  N56_N56_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => N56,
      O => N56_0
    );
  Mmux_calc_sqr_root_approx_v1132_SW0 : X_MUX2
    generic map(
      LOC => "SLICE_X14Y7"
    )
    port map (
      IA => N262,
      IB => '0',
      O => N56,
      SEL => rdy_cs_PWR_4_o_AND_2_o
    );
  Mmux_calc_sqr_root_approx_v1132_SW0_F : X_LUT6
    generic map(
      LOC => "SLICE_X14Y7",
      INIT => X"CC00000000000000"
    )
    port map (
      ADR0 => '1',
      ADR2 => '1',
      ADR3 => approx_cs(8),
      ADR4 => counter_cs(2),
      ADR1 => counter_cs(3),
      ADR5 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => N262
    );
  N1_2_C6LUT : X_LUT6
    generic map(
      LOC => "SLICE_X14Y7",
      INIT => X"0000000000000000"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR2 => '1',
      ADR3 => '1',
      ADR4 => '1',
      ADR5 => '1',
      O => NLW_N1_2_C6LUT_O_UNCONNECTED
    );
  fx_cs_3 : X_SFF
    generic map(
      LOC => "SLICE_X2Y6",
      INIT => '0'
    )
    port map (
      CE => Q_n0179_inv,
      CLK => NlwBufferSignal_fx_cs_3_CLK,
      I => NlwBufferSignal_fx_cs_3_IN,
      O => fx_cs(3),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  fx_cs_2 : X_SFF
    generic map(
      LOC => "SLICE_X2Y6",
      INIT => '0'
    )
    port map (
      CE => Q_n0179_inv,
      CLK => NlwBufferSignal_fx_cs_2_CLK,
      I => NlwBufferSignal_fx_cs_2_IN,
      O => fx_cs(2),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  fx_cs_1 : X_SFF
    generic map(
      LOC => "SLICE_X2Y6",
      INIT => '0'
    )
    port map (
      CE => Q_n0179_inv,
      CLK => NlwBufferSignal_fx_cs_1_CLK,
      I => NlwBufferSignal_fx_cs_1_IN,
      O => fx_cs(1),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  fx_cs_0 : X_SFF
    generic map(
      LOC => "SLICE_X2Y6",
      INIT => '0'
    )
    port map (
      CE => Q_n0179_inv,
      CLK => NlwBufferSignal_fx_cs_0_CLK,
      I => NlwBufferSignal_fx_cs_0_IN,
      O => fx_cs(0),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Sh25_Sh25_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => Sh25,
      O => Sh25_0
    );
  Sh251 : X_MUX2
    generic map(
      LOC => "SLICE_X2Y7"
    )
    port map (
      IA => N272,
      IB => N273,
      O => Sh25,
      SEL => counter_cs(1)
    );
  Sh251_F : X_LUT6
    generic map(
      LOC => "SLICE_X2Y7",
      INIT => X"4CFFCCCCCCFFCCCC"
    )
    port map (
      ADR1 => approx_cs(2),
      ADR2 => counter_cs(2),
      ADR4 => counter_cs(3),
      ADR0 => req_IBUF_0,
      ADR5 => rdy_cs_1782,
      ADR3 => counter_cs(0),
      O => N272
    );
  Sh251_G : X_LUT6
    generic map(
      LOC => "SLICE_X2Y7",
      INIT => X"30F0F5F0F0F0F5F0"
    )
    port map (
      ADR2 => approx_cs(4),
      ADR5 => rdy_cs_1782,
      ADR4 => counter_cs(3),
      ADR3 => counter_cs(2),
      ADR1 => req_IBUF_0,
      ADR0 => counter_cs(0),
      O => N273
    );
  Mmux_calc_sqr_root_approx_v41 : X_LUT6
    generic map(
      LOC => "SLICE_X2Y8",
      INIT => X"0000400000000000"
    )
    port map (
      ADR4 => counter_cs(0),
      ADR1 => counter_cs(1),
      ADR5 => counter_cs(3),
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => calc_sqr_root_calcPath_v,
      O => Mmux_calc_sqr_root_approx_v4
    );
  Mmux_calc_sqr_root_approx_v171_Mmux_calc_sqr_root_approx_v171_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => Sh110_pack_6,
      O => Sh110
    );
  Sh1101 : X_MUX2
    generic map(
      LOC => "SLICE_X2Y9"
    )
    port map (
      IA => N274,
      IB => N275,
      O => Sh110_pack_6,
      SEL => counter_cs(1)
    );
  Sh1101_F : X_LUT6
    generic map(
      LOC => "SLICE_X2Y9",
      INIT => X"3AAAFAAAFAAAFAAA"
    )
    port map (
      ADR2 => counter_cs(0),
      ADR3 => counter_cs(3),
      ADR1 => req_IBUF_0,
      ADR5 => rdy_cs_1782,
      ADR4 => counter_cs(2),
      ADR0 => approx_cs(1),
      O => N274
    );
  Sh1101_G : X_LUT6
    generic map(
      LOC => "SLICE_X2Y9",
      INIT => X"22FAAAAAAAFAAAAA"
    )
    port map (
      ADR0 => approx_cs(3),
      ADR1 => req_IBUF_0,
      ADR4 => counter_cs(2),
      ADR3 => counter_cs(3),
      ADR5 => rdy_cs_1782,
      ADR2 => counter_cs(0),
      O => N275
    );
  Mmux_calc_sqr_root_approx_v172 : X_LUT6
    generic map(
      LOC => "SLICE_X2Y9",
      INIT => X"CFCFC0C0FA0AFA0A"
    )
    port map (
      ADR2 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR3 => Sh25_0,
      ADR4 => Sh51,
      ADR1 => Sh61_0,
      ADR0 => Sh110,
      ADR5 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      O => Mmux_calc_sqr_root_approx_v171
    );
  Mmux_calc_sqr_root_approx_v51 : X_LUT6
    generic map(
      LOC => "SLICE_X3Y6",
      INIT => X"0400000000000000"
    )
    port map (
      ADR3 => counter_cs(0),
      ADR2 => counter_cs(1),
      ADR4 => counter_cs(3),
      ADR5 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR1 => calc_sqr_root_calcPath_v,
      O => Mmux_calc_sqr_root_approx_v5
    );
  approx_cs_2 : X_SFF
    generic map(
      LOC => "SLICE_X3Y7",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_approx_cs_2_CLK,
      I => calc_sqr_root_approx_v(2),
      O => approx_cs(2),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v54 : X_LUT6
    generic map(
      LOC => "SLICE_X3Y7",
      INIT => X"0A000E04FFF5FFF5"
    )
    port map (
      ADR0 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR5 => rdy_cs_PWR_4_o_AND_2_o_mmx_out12,
      ADR1 => Mmux_calc_sqr_root_approx_v5,
      ADR2 => N42,
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_2_0,
      ADR4 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      O => calc_sqr_root_approx_v(2)
    );
  approx_cs_1 : X_SFF
    generic map(
      LOC => "SLICE_X3Y7",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_approx_cs_1_CLK,
      I => calc_sqr_root_approx_v(1),
      O => approx_cs(1),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v44 : X_LUT6
    generic map(
      LOC => "SLICE_X3Y7",
      INIT => X"7474747455557755"
    )
    port map (
      ADR5 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR0 => rdy_cs_PWR_4_o_AND_2_o_mmx_out2,
      ADR3 => Mmux_calc_sqr_root_approx_v4,
      ADR1 => N42,
      ADR2 => GND_4_o_GND_4_o_sub_28_OUT_1_0,
      ADR4 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      O => calc_sqr_root_approx_v(1)
    );
  approx_cs_0 : X_SFF
    generic map(
      LOC => "SLICE_X3Y7",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_approx_cs_0_CLK,
      I => calc_sqr_root_approx_v(0),
      O => approx_cs(0),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v1413 : X_LUT6
    generic map(
      LOC => "SLICE_X3Y7",
      INIT => X"33333333BB11FB51"
    )
    port map (
      ADR0 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o_mmx_out15_0,
      ADR2 => Mmux_calc_sqr_root_approx_v1412_1878,
      ADR5 => N42,
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_0_0,
      ADR4 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      O => calc_sqr_root_approx_v(0)
    );
  Mmux_calc_sqr_root_approx_v1413_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X3Y7",
      INIT => X"5555555755555555"
    )
    port map (
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR2 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR0 => calc_sqr_root_calcPath_v,
      ADR5 => calc_sqr_root_corrNeg_v,
      O => N42
    );
  Mmux_calc_sqr_root_approx_v1412 : X_LUT6
    generic map(
      LOC => "SLICE_X3Y8",
      INIT => X"40004444FFFFFFFF"
    )
    port map (
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR1 => counter_cs(3),
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR2 => counter_cs(1),
      ADR3 => counter_cs(0),
      ADR5 => calc_sqr_root_calcPath_v,
      O => Mmux_calc_sqr_root_approx_v1412_1878
    );
  Mmux_x_cs_11_x_11_mux_14_OUT51 : X_LUT6
    generic map(
      LOC => "SLICE_X3Y9",
      INIT => X"FFFF0F0FFFFF0F0F"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR5 => '1',
      ADR3 => '1',
      ADR2 => approx_cs(2),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o11,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out12
    );
  Mmux_calc_sqr_root_approx_v19_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X4Y5",
      INIT => X"0088000000000000"
    )
    port map (
      ADR2 => '1',
      ADR0 => approx_cs(4),
      ADR4 => counter_cs(2),
      ADR1 => counter_cs(3),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => N52
    );
  Mmux_calc_sqr_root_approx_v6_Mmux_calc_sqr_root_approx_v6_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => Mmux_calc_sqr_root_approx_v181,
      O => Mmux_calc_sqr_root_approx_v181_0
    );
  Mmux_calc_sqr_root_approx_v182 : X_MUX2
    generic map(
      LOC => "SLICE_X4Y6"
    )
    port map (
      IA => N276,
      IB => N277,
      O => Mmux_calc_sqr_root_approx_v181,
      SEL => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q
    );
  Mmux_calc_sqr_root_approx_v182_F : X_LUT6
    generic map(
      LOC => "SLICE_X4Y6",
      INIT => X"0F0F0000BF8FB080"
    )
    port map (
      ADR2 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR5 => rdy_cs_PWR_4_o_AND_2_o1_2009,
      ADR1 => counter_cs(1),
      ADR0 => approx_cs(5),
      ADR3 => approx_cs(3),
      ADR4 => Sh25_0,
      O => N276
    );
  Mmux_calc_sqr_root_approx_v182_G : X_LUT6
    generic map(
      LOC => "SLICE_X4Y6",
      INIT => X"0C0C0F00AAAAAAAA"
    )
    port map (
      ADR5 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR2 => rdy_cs_PWR_4_o_AND_2_o11,
      ADR4 => counter_cs(1),
      ADR1 => approx_cs(9),
      ADR3 => approx_cs(7),
      ADR0 => Sh61_0,
      O => N277
    );
  Mmux_calc_sqr_root_approx_v61 : X_LUT6
    generic map(
      LOC => "SLICE_X4Y6",
      INIT => X"00000000000000C0"
    )
    port map (
      ADR0 => '1',
      ADR5 => counter_cs(0),
      ADR3 => counter_cs(1),
      ADR2 => counter_cs(3),
      ADR4 => counter_cs(2),
      ADR1 => calc_sqr_root_calcPath_v,
      O => Mmux_calc_sqr_root_approx_v6
    );
  Mmux_calc_sqr_root_approx_v3_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X4Y7",
      INIT => X"CD00000000000000"
    )
    port map (
      ADR2 => counter_cs(0),
      ADR0 => counter_cs(1),
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => calc_sqr_root_calcPath_v,
      ADR5 => GND_4_o_Decoder_30_OUT_10_3_11,
      O => N16
    );
  fx_cs_11 : X_SFF
    generic map(
      LOC => "SLICE_X4Y7",
      INIT => '0'
    )
    port map (
      CE => Q_n0179_inv,
      CLK => NlwBufferSignal_fx_cs_11_CLK,
      I => calc_sqr_root_approx_v(11),
      O => fx_cs(11),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v3 : X_LUT6
    generic map(
      LOC => "SLICE_X4Y7",
      INIT => X"AAAAAAAAE2E2AEAA"
    )
    port map (
      ADR4 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR0 => rdy_cs_PWR_4_o_AND_2_o_mmx_out22,
      ADR1 => calc_sqr_root_calcPath_v,
      ADR3 => N16,
      ADR5 => corrNeg_cs_counter_cs_3_AND_41_o,
      ADR2 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      O => calc_sqr_root_approx_v(11)
    );
  Mmux_calc_sqr_root_approx_v2_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X4Y7",
      INIT => X"BBBB0BFBBBBBBBBB"
    )
    port map (
      ADR1 => approx_cs(10),
      ADR2 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => calc_sqr_root_calcPath_v,
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_10_0,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N146
    );
  fx_cs_10 : X_SFF
    generic map(
      LOC => "SLICE_X4Y7",
      INIT => '0'
    )
    port map (
      CE => Q_n0179_inv,
      CLK => NlwBufferSignal_fx_cs_10_CLK,
      I => calc_sqr_root_approx_v(10),
      O => fx_cs(10),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v2 : X_LUT6
    generic map(
      LOC => "SLICE_X4Y7",
      INIT => X"0000FFFF0040BFFF"
    )
    port map (
      ADR0 => calc_sqr_root_index_v_0_0,
      ADR2 => calc_sqr_root_index_v(1),
      ADR1 => GND_4_o_Decoder_30_OUT_10_3_11,
      ADR5 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR3 => N147,
      ADR4 => N146,
      O => calc_sqr_root_approx_v(10)
    );
  Sh61_Sh61_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => Sh61,
      O => Sh61_0
    );
  Sh611 : X_MUX2
    generic map(
      LOC => "SLICE_X4Y8"
    )
    port map (
      IA => N268,
      IB => N269,
      O => Sh61,
      SEL => counter_cs(1)
    );
  Sh611_F : X_LUT6
    generic map(
      LOC => "SLICE_X4Y8",
      INIT => X"2AAFAAAAAAAFAAAA"
    )
    port map (
      ADR0 => approx_cs(6),
      ADR1 => req_IBUF_0,
      ADR4 => counter_cs(2),
      ADR3 => counter_cs(3),
      ADR2 => counter_cs(0),
      ADR5 => rdy_cs_1782,
      O => N268
    );
  Sh611_G : X_LUT6
    generic map(
      LOC => "SLICE_X4Y8",
      INIT => X"7700FF03FF00FF03"
    )
    port map (
      ADR3 => approx_cs(8),
      ADR5 => rdy_cs_1782,
      ADR4 => counter_cs(2),
      ADR1 => counter_cs(3),
      ADR0 => req_IBUF_0,
      ADR2 => counter_cs(0),
      O => N269
    );
  N22_N22_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => Sh51_pack_8,
      O => Sh51
    );
  Sh511 : X_MUX2
    generic map(
      LOC => "SLICE_X4Y9"
    )
    port map (
      IA => N270,
      IB => N271,
      O => Sh51_pack_8,
      SEL => counter_cs(1)
    );
  Sh511_F : X_LUT6
    generic map(
      LOC => "SLICE_X4Y9",
      INIT => X"7FFF0C00FFFF0C00"
    )
    port map (
      ADR4 => approx_cs(5),
      ADR5 => rdy_cs_1782,
      ADR1 => counter_cs(2),
      ADR3 => counter_cs(0),
      ADR0 => req_IBUF_0,
      ADR2 => counter_cs(3),
      O => N270
    );
  Sh511_G : X_LUT6
    generic map(
      LOC => "SLICE_X4Y9",
      INIT => X"44CCCCCCCCFCCCFC"
    )
    port map (
      ADR1 => approx_cs(7),
      ADR0 => req_IBUF_0,
      ADR3 => counter_cs(2),
      ADR5 => counter_cs(3),
      ADR4 => rdy_cs_1782,
      ADR2 => counter_cs(0),
      O => N271
    );
  Mmux_calc_sqr_root_approx_v110_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X4Y9",
      INIT => X"CCEECDEFDDFFCDEF"
    )
    port map (
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR5 => Sh51,
      ADR2 => Sh41,
      ADR3 => Sh9,
      O => N22
    );
  Sh92 : X_LUT6
    generic map(
      LOC => "SLICE_X4Y9",
      INIT => X"00002E2E00003F0C"
    )
    port map (
      ADR3 => approx_cs(8),
      ADR0 => approx_cs(10),
      ADR1 => counter_cs(0),
      ADR5 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR2 => N80,
      ADR4 => rdy_cs_PWR_4_o_AND_2_o11,
      O => Sh9
    );
  Mmux_calc_sqr_root_approx_v19_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X5Y6",
      INIT => X"CCCDCCEFFFCDFFEF"
    )
    port map (
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR2 => Sh31,
      ADR4 => Sh41,
      ADR5 => Sh8,
      O => N20
    );
  Sh81 : X_LUT6
    generic map(
      LOC => "SLICE_X5Y6",
      INIT => X"00000000EE44F0F0"
    )
    port map (
      ADR1 => approx_cs(8),
      ADR3 => approx_cs(10),
      ADR4 => counter_cs(0),
      ADR2 => N186,
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR5 => rdy_cs_PWR_4_o_AND_2_o,
      O => Sh8
    );
  rdy_cs_PWR_4_o_AND_2_o_mmx_out19_rdy_cs_PWR_4_o_AND_2_o_mmx_out19_AMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => rdy_cs_PWR_4_o_AND_2_o_mmx_out15,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out15_0
    );
  Mmux_x_cs_11_x_11_mux_14_OUT121 : X_LUT6
    generic map(
      LOC => "SLICE_X5Y7",
      INIT => X"FF00FFFFFF00FFFF"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR2 => '1',
      ADR5 => '1',
      ADR4 => approx_cs(8),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o11,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out19
    );
  Sh911 : X_LUT6
    generic map(
      LOC => "SLICE_X5Y7",
      INIT => X"0000CCFF0000CC00"
    )
    port map (
      ADR0 => '1',
      ADR2 => '1',
      ADR5 => approx_cs(8),
      ADR1 => approx_cs(10),
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR4 => rdy_cs_PWR_4_o_AND_2_o11,
      O => Sh91
    );
  Mmux_x_cs_11_x_11_mux_14_OUT191 : X_LUT6
    generic map(
      LOC => "SLICE_X5Y7",
      INIT => X"CACACACACACACACA"
    )
    port map (
      ADR4 => '1',
      ADR3 => '1',
      ADR1 => x_4_IBUF_0,
      ADR0 => x_cs(4),
      ADR2 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => '1',
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out5
    );
  Mmux_x_cs_11_x_11_mux_14_OUT81 : X_LUT5
    generic map(
      LOC => "SLICE_X5Y7",
      INIT => X"F0F0FFFF"
    )
    port map (
      ADR0 => '1',
      ADR4 => approx_cs(0),
      ADR1 => '1',
      ADR3 => '1',
      ADR2 => rdy_cs_PWR_4_o_AND_2_o,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out15
    );
  Mmux_calc_sqr_root_approx_v1211_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X5Y8",
      INIT => X"55F5555500A00000"
    )
    port map (
      ADR1 => '1',
      ADR4 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR5 => rdy_cs_PWR_4_o_AND_2_o_mmx_out5,
      ADR2 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out4,
      ADR0 => calc_sqr_root_calcPath_v,
      ADR3 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N91
    );
  Mmux_x_cs_11_x_11_mux_14_OUT111 : X_LUT6
    generic map(
      LOC => "SLICE_X5Y8",
      INIT => X"FFFFFFFF0000FFFF"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR2 => '1',
      ADR3 => '1',
      ADR4 => approx_cs(7),
      ADR5 => rdy_cs_PWR_4_o_AND_2_o11,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out18
    );
  Q_n0179_inv1 : X_LUT6
    generic map(
      LOC => "SLICE_X5Y9",
      INIT => X"A200A200A2000000"
    )
    port map (
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR2 => rdy_cs_PWR_4_o_AND_2_o,
      ADR1 => rdy_cs_1782,
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR5 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      O => Q_n0179_inv
    );
  Mmux_calc_sqr_root_n_v_3_0_21 : X_LUT6
    generic map(
      LOC => "SLICE_X5Y9",
      INIT => X"44CCCCCCCCCCCCCC"
    )
    port map (
      ADR2 => '1',
      ADR3 => req_IBUF_0,
      ADR5 => counter_cs_3_2_1780,
      ADR0 => counter_cs_2_2_1781,
      ADR1 => counter_cs_1_2_1778,
      ADR4 => rdy_cs_2_1794,
      O => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q
    );
  Mmux_x_cs_11_x_11_mux_14_OUT131 : X_LUT6
    generic map(
      LOC => "SLICE_X5Y10",
      INIT => X"FFFF00FFFFFF00FF"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR2 => '1',
      ADR5 => '1',
      ADR3 => approx_cs(1),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o11,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out2
    );
  Mmux_calc_sqr_root_approx_v9_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y4",
      INIT => X"FFFFFFFFF7F7FFFF"
    )
    port map (
      ADR3 => '1',
      ADR1 => GND_4_o_Decoder_30_OUT_4_3_11_0,
      ADR0 => calc_sqr_root_calcPath_v,
      ADR5 => calc_sqr_root_index_v_0_0,
      ADR4 => calc_sqr_root_index_v(1),
      ADR2 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N71
    );
  Mmux_calc_sqr_root_approx_v9 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y4",
      INIT => X"80DF80DF80DFD5DF"
    )
    port map (
      ADR0 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR3 => rdy_cs_PWR_4_o_AND_2_o_mmx_out17,
      ADR2 => N63,
      ADR1 => GND_4_o_GND_4_o_sub_28_OUT_6_0,
      ADR5 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR4 => N71,
      O => calc_sqr_root_approx_v(6)
    );
  x_cs_11_GND_4_o_mux_26_OUT_7_x_cs_11_GND_4_o_mux_26_OUT_7_BMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_index_v(0),
      O => calc_sqr_root_index_v_0_0
    );
  Mmux_calc_sqr_root_approx_v1361 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y5",
      INIT => X"AFAFAFA0A0A0AFA0"
    )
    port map (
      ADR1 => '1',
      ADR5 => x_7_IBUF_0,
      ADR3 => x_cs(7),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR0 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out7,
      ADR2 => Mmux_calc_sqr_root_calcPath_v181,
      O => x_cs_11_GND_4_o_mux_26_OUT_7_Q
    );
  Mmux_calc_sqr_root_approx_v1351 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y5",
      INIT => X"F5F5F5A0A0A0F5A0"
    )
    port map (
      ADR1 => '1',
      ADR5 => x_6_IBUF_0,
      ADR3 => x_cs(6),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out6,
      ADR0 => Mmux_calc_sqr_root_calcPath_v181,
      O => x_cs_11_GND_4_o_mux_26_OUT_6_Q
    );
  Mmux_calc_sqr_root_corrNeg_v121 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y5",
      INIT => X"A000F000A000F000"
    )
    port map (
      ADR1 => '1',
      ADR4 => counter_cs(1),
      ADR2 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => Mmux_calc_sqr_root_calcPath_v181,
      ADR5 => '1',
      O => calc_sqr_root_index_v(1)
    );
  Mmux_calc_sqr_root_corrNeg_v111 : X_LUT5
    generic map(
      LOC => "SLICE_X6Y5",
      INIT => X"B000B000"
    )
    port map (
      ADR1 => counter_cs(0),
      ADR4 => '1',
      ADR2 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => Mmux_calc_sqr_root_calcPath_v181,
      O => calc_sqr_root_index_v(0)
    );
  Mmux_calc_sqr_root_calcPath_v18_2 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y5",
      INIT => X"FFFFFEFFFF00FE00"
    )
    port map (
      ADR4 => Mmux_calc_sqr_root_calcPath_v11_1915,
      ADR2 => x_5_IBUF_0,
      ADR1 => x_4_IBUF_0,
      ADR0 => Mmux_calc_sqr_root_calcPath_v1,
      ADR3 => rdy_cs_PWR_4_o_AND_2_o1_2009,
      ADR5 => Mmux_calc_sqr_root_calcPath_v17_2022,
      O => Mmux_calc_sqr_root_calcPath_v181
    );
  Sh10_Sh10_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => N24,
      O => N24_0
    );
  Mmux_calc_sqr_root_approx_v111_SW0 : X_MUX2
    generic map(
      LOC => "SLICE_X6Y6"
    )
    port map (
      IA => N258,
      IB => N259,
      O => N24,
      SEL => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q
    );
  Mmux_calc_sqr_root_approx_v111_SW0_F : X_LUT6
    generic map(
      LOC => "SLICE_X6Y6",
      INIT => X"FFFF55F0FFFF55CC"
    )
    port map (
      ADR5 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o_mmx_out16,
      ADR2 => rdy_cs_PWR_4_o_AND_2_o_mmx_out18,
      ADR0 => Sh10,
      O => N258
    );
  Mmux_calc_sqr_root_approx_v111_SW0_G : X_LUT6
    generic map(
      LOC => "SLICE_X6Y6",
      INIT => X"F0FCF0FAFFFCFFFA"
    )
    port map (
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR2 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o_mmx_out19,
      ADR0 => rdy_cs_PWR_4_o_AND_2_o_mmx_out17,
      ADR5 => Sh10,
      O => N259
    );
  Sh101 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y6",
      INIT => X"00300030000000B8"
    )
    port map (
      ADR2 => approx_cs(9),
      ADR0 => approx_cs(10),
      ADR1 => counter_cs(0),
      ADR4 => counter_cs(1),
      ADR5 => N162,
      ADR3 => rdy_cs_PWR_4_o_AND_2_o11,
      O => Sh10
    );
  Mmux_x_cs_11_x_11_mux_14_OUT101 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y6",
      INIT => X"FFFF0F0FFFFF0F0F"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR5 => '1',
      ADR3 => '1',
      ADR2 => approx_cs(6),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o11,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out17
    );
  fx_cs_9_fx_cs_9_BMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => rdy_cs_PWR_4_o_AND_2_o_mmx_out22_pack_4,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out22
    );
  fx_cs_9 : X_SFF
    generic map(
      LOC => "SLICE_X6Y7",
      INIT => '0'
    )
    port map (
      CE => Q_n0179_inv,
      CLK => NlwBufferSignal_fx_cs_9_CLK,
      I => NlwBufferSignal_fx_cs_9_IN,
      O => fx_cs(9),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v2_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y7",
      INIT => X"CFEF0020CFEFCFEF"
    )
    port map (
      ADR5 => approx_cs(10),
      ADR0 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_10_0,
      ADR1 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N147
    );
  fx_cs_8 : X_SFF
    generic map(
      LOC => "SLICE_X6Y7",
      INIT => '0'
    )
    port map (
      CE => Q_n0179_inv,
      CLK => NlwBufferSignal_fx_cs_8_CLK,
      I => NlwBufferSignal_fx_cs_8_IN,
      O => fx_cs(8),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  corrNeg_cs_counter_cs_3_AND_41_o1_2 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y7",
      INIT => X"0001000100000000"
    )
    port map (
      ADR4 => '1',
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR2 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR5 => calc_sqr_root_corrNeg_v,
      O => corrNeg_cs_counter_cs_3_AND_41_o11
    );
  fx_cs_7 : X_SFF
    generic map(
      LOC => "SLICE_X6Y7",
      INIT => '0'
    )
    port map (
      CE => Q_n0179_inv,
      CLK => NlwBufferSignal_fx_cs_7_CLK,
      I => NlwBufferSignal_fx_cs_7_IN,
      O => fx_cs(7),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_x_cs_11_x_11_mux_14_OUT141 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y7",
      INIT => X"FF0FFF0FFF0FFF0F"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR4 => '1',
      ADR2 => approx_cs(9),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => '1',
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out20
    );
  Mmux_x_cs_11_x_11_mux_14_OUT163 : X_LUT5
    generic map(
      LOC => "SLICE_X6Y7",
      INIT => X"CCAA00AA"
    )
    port map (
      ADR1 => Mmux_x_cs_11_x_11_mux_14_OUT161_1903,
      ADR4 => Mmux_x_cs_11_x_11_mux_14_OUT16,
      ADR0 => approx_cs(11),
      ADR2 => '1',
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out22_pack_4
    );
  fx_cs_6 : X_SFF
    generic map(
      LOC => "SLICE_X6Y7",
      INIT => '0'
    )
    port map (
      CE => Q_n0179_inv,
      CLK => NlwBufferSignal_fx_cs_6_CLK,
      I => NlwBufferSignal_fx_cs_6_IN,
      O => fx_cs(6),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_11_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y7",
      INIT => X"9180918091809180"
    )
    port map (
      ADR5 => '1',
      ADR4 => '1',
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR3 => Sh11,
      ADR2 => rdy_cs_PWR_4_o_AND_2_o_mmx_out22,
      O => N213
    );
  Mmux_calc_sqr_root_approx_v112_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y8",
      INIT => X"AFAFAFAFAABBFFBB"
    )
    port map (
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR5 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR2 => Sh11,
      ADR1 => Sh61_0,
      ADR4 => Sh71,
      O => N26
    );
  Sh711 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y8",
      INIT => X"00000000B8B8B8B8"
    )
    port map (
      ADR3 => '1',
      ADR4 => '1',
      ADR2 => approx_cs(7),
      ADR0 => approx_cs(9),
      ADR1 => counter_cs(1),
      ADR5 => rdy_cs_PWR_4_o_AND_2_o11,
      O => Sh71
    );
  Mmux_calc_sqr_root_corrNeg_v1_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y8",
      INIT => X"FFFFFFFFFFFF0000"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR2 => '1',
      ADR3 => '1',
      ADR5 => counter_cs(0),
      ADR4 => counter_cs(1),
      O => N18
    );
  Sh111 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y8",
      INIT => X"80FF800300FF0003"
    )
    port map (
      ADR0 => req_IBUF_0,
      ADR4 => approx_cs(10),
      ADR5 => rdy_cs_1782,
      ADR1 => counter_cs(2),
      ADR2 => counter_cs(3),
      ADR3 => N18,
      O => Sh11
    );
  Mmux_calc_sqr_root_approx_v183 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y9",
      INIT => X"2070202020202020"
    )
    port map (
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR5 => Sh11,
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o_mmx_out13,
      ADR3 => x_cs_11_x_11_mux_14_OUT_11_Q,
      ADR2 => Mmux_calc_sqr_root_calcPath_v18_1894,
      O => Mmux_calc_sqr_root_approx_v182_1940
    );
  Mmux_calc_sqr_root_approx_v131_SW2_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y9",
      INIT => X"F5F5FFFFFFFFFFFF"
    )
    port map (
      ADR3 => '1',
      ADR1 => '1',
      ADR5 => counter_cs(3),
      ADR4 => counter_cs(2),
      ADR0 => approx_cs(0),
      ADR2 => rdy_cs_PWR_4_o_AND_2_o11,
      O => N200
    );
  Mmux_calc_sqr_root_approx_v173 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y9",
      INIT => X"0F0F220000000000"
    )
    port map (
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR2 => rdy_cs_PWR_4_o_AND_2_o_mmx_out12,
      ADR0 => Sh10,
      ADR1 => x_cs_11_x_11_mux_14_OUT_11_Q,
      ADR5 => Mmux_calc_sqr_root_calcPath_v18_1894,
      O => Mmux_calc_sqr_root_approx_v172_1935
    );
  Mmux_calc_sqr_root_calcPath_v18_1 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y9",
      INIT => X"FFFFFEFFFF00FE00"
    )
    port map (
      ADR4 => Mmux_calc_sqr_root_calcPath_v11_1915,
      ADR1 => x_5_IBUF_0,
      ADR2 => x_4_IBUF_0,
      ADR0 => Mmux_calc_sqr_root_calcPath_v1,
      ADR3 => rdy_cs_PWR_4_o_AND_2_o1_2009,
      ADR5 => Mmux_calc_sqr_root_calcPath_v17_2022,
      O => Mmux_calc_sqr_root_calcPath_v18_1894
    );
  rdy_cs_PWR_4_o_AND_2_o_mmx_out13_rdy_cs_PWR_4_o_AND_2_o_mmx_out13_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => Mmux_calc_sqr_root_approx_v162,
      O => Mmux_calc_sqr_root_approx_v162_0
    );
  Mmux_calc_sqr_root_approx_v163 : X_MUX2
    generic map(
      LOC => "SLICE_X6Y10"
    )
    port map (
      IA => N266,
      IB => N267,
      O => Mmux_calc_sqr_root_approx_v162,
      SEL => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q
    );
  Mmux_calc_sqr_root_approx_v163_F : X_LUT6
    generic map(
      LOC => "SLICE_X6Y10",
      INIT => X"FFFF0D0800000D08"
    )
    port map (
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR2 => rdy_cs_PWR_4_o_AND_2_o,
      ADR0 => counter_cs(1),
      ADR1 => approx_cs(2),
      ADR3 => approx_cs(0),
      ADR5 => Sh110,
      O => N266
    );
  Mmux_calc_sqr_root_approx_v163_G : X_LUT6
    generic map(
      LOC => "SLICE_X6Y10",
      INIT => X"F0F00000F0F0CCAA"
    )
    port map (
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR5 => rdy_cs_PWR_4_o_AND_2_o1_2009,
      ADR3 => counter_cs(1),
      ADR1 => approx_cs(6),
      ADR0 => approx_cs(4),
      ADR2 => Sh51,
      O => N267
    );
  Mmux_x_cs_11_x_11_mux_14_OUT61 : X_LUT6
    generic map(
      LOC => "SLICE_X6Y10",
      INIT => X"FFFFFFFF00FF00FF"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR2 => '1',
      ADR4 => '1',
      ADR3 => approx_cs(3),
      ADR5 => rdy_cs_PWR_4_o_AND_2_o11,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out13
    );
  Mmux_calc_sqr_root_approx_v112_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y4",
      INIT => X"E21DFFFF00FF1D1D"
    )
    port map (
      ADR2 => x_7_IBUF_0,
      ADR0 => x_cs(7),
      ADR1 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out7,
      ADR4 => Mmux_calc_sqr_root_calcPath_v18_1894,
      ADR5 => Mmux_calc_sqr_root_approx_v1101,
      O => N167
    );
  counter_cs_3_PWR_4_o_LessThan_16_o91 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y4",
      INIT => X"0033000A33330A0A"
    )
    port map (
      ADR1 => N194_0,
      ADR0 => x_cs(7),
      ADR2 => x_cs(11),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o11,
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR5 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      O => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out7
    );
  Mmux_calc_sqr_root_approx_v112_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y4",
      INIT => X"FFBB038BFCB80088"
    )
    port map (
      ADR0 => x_7_IBUF_0,
      ADR3 => N209_0,
      ADR5 => x_cs(7),
      ADR1 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => Mmux_calc_sqr_root_calcPath_v16_1850,
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out7,
      O => N168
    );
  Mmux_calc_sqr_root_approx_v1241_SW3 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y5",
      INIT => X"FEFEF4F4FE0EF404"
    )
    port map (
      ADR4 => x_7_IBUF_0,
      ADR1 => x_cs(7),
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out7,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR5 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N109
    );
  Mmux_calc_sqr_root_approx_v8_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y5",
      INIT => X"FFFFDFFFFFFFDFFF"
    )
    port map (
      ADR5 => '1',
      ADR1 => calc_sqr_root_index_v(1),
      ADR0 => calc_sqr_root_index_v_0_0,
      ADR3 => GND_4_o_Decoder_30_OUT_4_3_11_0,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N74
    );
  fx_cs_5 : X_SFF
    generic map(
      LOC => "SLICE_X7Y5",
      INIT => '0'
    )
    port map (
      CE => Q_n0179_inv,
      CLK => NlwBufferSignal_fx_cs_5_CLK,
      I => calc_sqr_root_approx_v(5),
      O => fx_cs(5),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v8 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y5",
      INIT => X"AA000303FF55FFFF"
    )
    port map (
      ADR4 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR5 => rdy_cs_PWR_4_o_AND_2_o_mmx_out16,
      ADR0 => N63,
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_5_0,
      ADR2 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR1 => N74,
      O => calc_sqr_root_approx_v(5)
    );
  fx_cs_4 : X_SFF
    generic map(
      LOC => "SLICE_X7Y5",
      INIT => '0'
    )
    port map (
      CE => Q_n0179_inv,
      CLK => NlwBufferSignal_fx_cs_4_CLK,
      I => NlwBufferSignal_fx_cs_4_IN,
      O => fx_cs(4),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_x_cs_11_x_11_mux_14_OUT91 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y5",
      INIT => X"FFFFFFFF00FF00FF"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR2 => '1',
      ADR4 => '1',
      ADR3 => approx_cs(5),
      ADR5 => rdy_cs_PWR_4_o_AND_2_o11,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out16
    );
  approx_cs_6 : X_SFF
    generic map(
      LOC => "SLICE_X7Y6",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_approx_cs_6_CLK,
      I => NlwBufferSignal_approx_cs_6_IN,
      O => approx_cs(6),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v12_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y6",
      INIT => X"FFFF0000FFEF0000"
    )
    port map (
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR5 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR4 => calc_sqr_root_calcPath_v,
      ADR2 => calc_sqr_root_corrNeg_v,
      O => N63
    );
  approx_cs_5 : X_SFF
    generic map(
      LOC => "SLICE_X7Y6",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_approx_cs_5_CLK,
      I => NlwBufferSignal_approx_cs_5_IN,
      O => approx_cs(5),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v7_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y6",
      INIT => X"FFFFFBFBFFFFFFFF"
    )
    port map (
      ADR3 => '1',
      ADR1 => GND_4_o_Decoder_30_OUT_4_3_11_0,
      ADR5 => calc_sqr_root_calcPath_v,
      ADR2 => calc_sqr_root_index_v(1),
      ADR4 => calc_sqr_root_index_v_0_0,
      ADR0 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N77
    );
  approx_cs_4 : X_SFF
    generic map(
      LOC => "SLICE_X7Y6",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_approx_cs_4_CLK,
      I => calc_sqr_root_approx_v(4),
      O => approx_cs(4),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v7 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y6",
      INIT => X"8F8F8FDF07070757"
    )
    port map (
      ADR0 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR2 => rdy_cs_PWR_4_o_AND_2_o_mmx_out14,
      ADR1 => N63,
      ADR5 => GND_4_o_GND_4_o_sub_28_OUT_4_0,
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR4 => N77,
      O => calc_sqr_root_approx_v(4)
    );
  approx_cs_3 : X_SFF
    generic map(
      LOC => "SLICE_X7Y6",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_approx_cs_3_CLK,
      I => calc_sqr_root_approx_v(3),
      O => approx_cs(3),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v64 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y6",
      INIT => X"33333333BF15BB11"
    )
    port map (
      ADR0 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o_mmx_out13,
      ADR4 => Mmux_calc_sqr_root_approx_v6,
      ADR5 => N42,
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_3_0,
      ADR2 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      O => calc_sqr_root_approx_v(3)
    );
  Mmux_calc_sqr_root_approx_v11_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y7",
      INIT => X"FFFFFFBBFFFFFFFF"
    )
    port map (
      ADR2 => '1',
      ADR5 => calc_sqr_root_calcPath_v,
      ADR1 => GND_4_o_Decoder_30_OUT_10_3_11,
      ADR3 => calc_sqr_root_index_v(1),
      ADR4 => calc_sqr_root_index_v_0_0,
      ADR0 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N68
    );
  approx_cs_8 : X_SFF
    generic map(
      LOC => "SLICE_X7Y7",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_approx_cs_8_CLK,
      I => calc_sqr_root_approx_v(8),
      O => approx_cs(8),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v11 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y7",
      INIT => X"B313B313B313BF1F"
    )
    port map (
      ADR2 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o_mmx_out19,
      ADR0 => N63,
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_8_0,
      ADR4 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR5 => N68,
      O => calc_sqr_root_approx_v(8)
    );
  Mmux_calc_sqr_root_approx_v10_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y7",
      INIT => X"FF7FFF7FFFFFFFFF"
    )
    port map (
      ADR4 => '1',
      ADR2 => calc_sqr_root_index_v(1),
      ADR5 => calc_sqr_root_index_v_0_0,
      ADR1 => GND_4_o_Decoder_30_OUT_4_3_11_0,
      ADR0 => calc_sqr_root_calcPath_v,
      ADR3 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N65
    );
  approx_cs_7 : X_SFF
    generic map(
      LOC => "SLICE_X7Y7",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_approx_cs_7_CLK,
      I => calc_sqr_root_approx_v(7),
      O => approx_cs(7),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v10 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y7",
      INIT => X"808080D5DFDFDFDF"
    )
    port map (
      ADR0 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR5 => rdy_cs_PWR_4_o_AND_2_o_mmx_out18,
      ADR2 => N63,
      ADR1 => GND_4_o_GND_4_o_sub_28_OUT_7_0,
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR4 => N65,
      O => calc_sqr_root_approx_v(7)
    );
  Mmux_calc_sqr_root_approx_v144_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y8",
      INIT => X"0A00000000000000"
    )
    port map (
      ADR1 => '1',
      ADR0 => approx_cs(10),
      ADR4 => counter_cs(2),
      ADR5 => counter_cs(3),
      ADR2 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => Mmux_calc_sqr_root_calcPath_v18_1894,
      O => N60
    );
  approx_cs_11 : X_SFF
    generic map(
      LOC => "SLICE_X7Y8",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_approx_cs_11_CLK,
      I => NlwBufferSignal_approx_cs_11_IN,
      O => approx_cs(11),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Sh711_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y8",
      INIT => X"FF00F0F0FF00F0F0"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR5 => '1',
      ADR2 => approx_cs(7),
      ADR3 => approx_cs(9),
      ADR4 => counter_cs(1),
      O => N186
    );
  approx_cs_10 : X_SFF
    generic map(
      LOC => "SLICE_X7Y8",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_approx_cs_10_CLK,
      I => NlwBufferSignal_approx_cs_10_IN,
      O => approx_cs(10),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v12_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y8",
      INIT => X"FFFFBBFFFFFFFFFF"
    )
    port map (
      ADR2 => '1',
      ADR0 => calc_sqr_root_index_v(1),
      ADR5 => calc_sqr_root_index_v_0_0,
      ADR1 => GND_4_o_Decoder_30_OUT_10_3_11,
      ADR3 => calc_sqr_root_calcPath_v,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N62
    );
  approx_cs_9 : X_SFF
    generic map(
      LOC => "SLICE_X7Y8",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_approx_cs_9_CLK,
      I => calc_sqr_root_approx_v(9),
      O => approx_cs(9),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v12 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y8",
      INIT => X"F555F57705550577"
    )
    port map (
      ADR3 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR0 => rdy_cs_PWR_4_o_AND_2_o_mmx_out20,
      ADR2 => N63,
      ADR5 => GND_4_o_GND_4_o_sub_28_OUT_9_0,
      ADR1 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR4 => N62,
      O => calc_sqr_root_approx_v(9)
    );
  Sh311 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y9",
      INIT => X"0000FF000000F0F0"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR2 => approx_cs(3),
      ADR3 => approx_cs(5),
      ADR5 => counter_cs(1),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o1_2009,
      O => Sh31
    );
  Mmux_calc_sqr_root_approx_v134 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y9",
      INIT => X"3010303000000000"
    )
    port map (
      ADR0 => counter_cs(3),
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR1 => x_cs_11_x_11_mux_14_OUT_11_Q,
      ADR5 => Mmux_calc_sqr_root_approx_v132_2059,
      ADR2 => Mmux_calc_sqr_root_calcPath_v18_1894,
      O => Mmux_calc_sqr_root_approx_v133_1926
    );
  Mmux_calc_sqr_root_approx_v133 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y9",
      INIT => X"0F0D0A08050D0008"
    )
    port map (
      ADR2 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR4 => Mmux_calc_sqr_root_approx_v131_2021,
      ADR1 => Sh31,
      ADR5 => Sh41,
      O => Mmux_calc_sqr_root_approx_v132_2059
    );
  Sh411 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y9",
      INIT => X"00F300F300C000C0"
    )
    port map (
      ADR0 => '1',
      ADR4 => '1',
      ADR2 => approx_cs(6),
      ADR5 => approx_cs(4),
      ADR1 => counter_cs(1),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o1_2009,
      O => Sh41
    );
  counter_cs_3 : X_SFF
    generic map(
      LOC => "SLICE_X7Y10",
      INIT => '1'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_counter_cs_3_CLK,
      I => NlwBufferSignal_counter_cs_3_IN,
      O => counter_cs(3),
      SSET => nRes_inv,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  Sh92_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y10",
      INIT => X"FF0FFF0FFF0AFF0A"
    )
    port map (
      ADR4 => '1',
      ADR1 => '1',
      ADR2 => approx_cs(9),
      ADR5 => counter_cs(3),
      ADR3 => counter_cs(1),
      ADR0 => counter_cs(2),
      O => N80
    );
  counter_cs_2 : X_SFF
    generic map(
      LOC => "SLICE_X7Y10",
      INIT => '1'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_counter_cs_2_CLK,
      I => calc_sqr_root_counter_v(2),
      O => counter_cs(2),
      SSET => nRes_inv,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  Mmux_calc_sqr_root_counter_v31 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y10",
      INIT => X"BACFFF00FF00FF00"
    )
    port map (
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR5 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR2 => rdy_cs_1782,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o,
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      O => calc_sqr_root_counter_v(2)
    );
  counter_cs_1 : X_SFF
    generic map(
      LOC => "SLICE_X7Y10",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_counter_cs_1_CLK,
      I => calc_sqr_root_counter_v(1),
      O => counter_cs(1),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_counter_v21 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y10",
      INIT => X"C3CCC9CC33CC99CC"
    )
    port map (
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR0 => rdy_cs_1782,
      ADR5 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR2 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      O => calc_sqr_root_counter_v(1)
    );
  counter_cs_0 : X_SFF
    generic map(
      LOC => "SLICE_X7Y10",
      INIT => '1'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_counter_cs_0_CLK,
      I => NlwBufferSignal_counter_cs_0_IN,
      O => counter_cs(0),
      SSET => nRes_inv,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  Sh101_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y10",
      INIT => X"CC00000000000000"
    )
    port map (
      ADR0 => '1',
      ADR2 => '1',
      ADR4 => req_IBUF_0,
      ADR5 => rdy_cs_1782,
      ADR3 => counter_cs(2),
      ADR1 => counter_cs(3),
      O => N162
    );
  Mmux_calc_sqr_root_approx_v132 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y11",
      INIT => X"0000FA8800005088"
    )
    port map (
      ADR2 => approx_cs(1),
      ADR1 => approx_cs(0),
      ADR5 => approx_cs(2),
      ADR0 => counter_cs(0),
      ADR3 => counter_cs(1),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o1_2009,
      O => Mmux_calc_sqr_root_approx_v131_2021
    );
  rdy_cs_PWR_4_o_AND_2_o1_1 : X_LUT6
    generic map(
      LOC => "SLICE_X7Y11",
      INIT => X"8080000080000000"
    )
    port map (
      ADR0 => req_IBUF_0,
      ADR1 => rdy_cs_1_1799,
      ADR5 => counter_cs_0_1_1796,
      ADR3 => counter_cs_1_1_1795,
      ADR2 => counter_cs_2_1_1798,
      ADR4 => counter_cs_3_1_1797,
      O => rdy_cs_PWR_4_o_AND_2_o1_2009
    );
  N192_N192_AMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => N194,
      O => N194_0
    );
  Mmux_x_cs_11_x_11_mux_14_OUT211_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X8Y3",
      INIT => X"CFCFCFCFCFCFCFCF"
    )
    port map (
      ADR0 => '1',
      ADR4 => '1',
      ADR3 => '1',
      ADR2 => x_6_IBUF_0,
      ADR1 => x_11_IBUF_0,
      ADR5 => '1',
      O => N192
    );
  Mmux_x_cs_11_x_11_mux_14_OUT221_SW0 : X_LUT5
    generic map(
      LOC => "SLICE_X8Y3",
      INIT => X"CCFFCCFF"
    )
    port map (
      ADR0 => '1',
      ADR4 => '1',
      ADR3 => x_7_IBUF_0,
      ADR2 => '1',
      ADR1 => x_11_IBUF_0,
      O => N194
    );
  Mmux_calc_sqr_root_approx_v1301 : X_LUT6
    generic map(
      LOC => "SLICE_X8Y4",
      INIT => X"555500000CCC0CCC"
    )
    port map (
      ADR4 => x_1_IBUF_0,
      ADR1 => x_cs(1),
      ADR5 => rdy_cs_PWR_4_o_AND_2_o,
      ADR0 => N217,
      ADR2 => N218,
      ADR3 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => x_cs_11_GND_4_o_mux_26_OUT_1_Q
    );
  x_cs_5 : X_SFF
    generic map(
      LOC => "SLICE_X8Y8",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_x_cs_5_CLK,
      I => calc_sqr_root_x_v(5),
      O => x_cs(5),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v1221 : X_LUT6
    generic map(
      LOC => "SLICE_X8Y8",
      INIT => X"FFCC00CCF0AAF0AA"
    )
    port map (
      ADR5 => GND_4_o_GND_4_o_sub_28_OUT_5_0,
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR4 => N99,
      ADR0 => N96,
      ADR1 => N97,
      ADR2 => N98,
      O => calc_sqr_root_x_v(5)
    );
  Mmux_calc_sqr_root_approx_v1221_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X8Y8",
      INIT => X"54045404FEAE5404"
    )
    port map (
      ADR3 => x_5_IBUF_0,
      ADR1 => x_cs(5),
      ADR2 => rdy_cs_PWR_4_o_AND_2_o,
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out5,
      ADR0 => calc_sqr_root_calcPath_v,
      ADR5 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N98
    );
  x_cs_4 : X_SFF
    generic map(
      LOC => "SLICE_X8Y8",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_x_cs_4_CLK,
      I => calc_sqr_root_x_v(4),
      O => x_cs(4),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v1211 : X_LUT6
    generic map(
      LOC => "SLICE_X8Y8",
      INIT => X"F5A0DDDDF5A08888"
    )
    port map (
      ADR0 => GND_4_o_GND_4_o_sub_28_OUT_4_0,
      ADR4 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR2 => N94,
      ADR5 => N91,
      ADR1 => N92,
      ADR3 => N93,
      O => calc_sqr_root_x_v(4)
    );
  Mmux_calc_sqr_root_approx_v1211_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X8Y8",
      INIT => X"0000FF00E4E4E4E4"
    )
    port map (
      ADR2 => x_4_IBUF_0,
      ADR1 => x_cs(4),
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out4,
      ADR5 => calc_sqr_root_calcPath_v,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N93
    );
  N94_N94_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => N58,
      O => N58_0
    );
  Mmux_calc_sqr_root_approx_v1141_SW0 : X_MUX2
    generic map(
      LOC => "SLICE_X8Y9"
    )
    port map (
      IA => N260,
      IB => '0',
      O => N58,
      SEL => rdy_cs_PWR_4_o_AND_2_o
    );
  Mmux_calc_sqr_root_approx_v1141_SW0_F : X_LUT6
    generic map(
      LOC => "SLICE_X8Y9",
      INIT => X"CC00000000000000"
    )
    port map (
      ADR0 => '1',
      ADR2 => '1',
      ADR1 => approx_cs(9),
      ADR4 => counter_cs(2),
      ADR5 => counter_cs(3),
      ADR3 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => N260
    );
  N1_C6LUT : X_LUT6
    generic map(
      LOC => "SLICE_X8Y9",
      INIT => X"0000000000000000"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR2 => '1',
      ADR3 => '1',
      ADR4 => '1',
      ADR5 => '1',
      O => NLW_N1_C6LUT_O_UNCONNECTED
    );
  Mmux_calc_sqr_root_approx_v1211_SW3 : X_LUT6
    generic map(
      LOC => "SLICE_X8Y9",
      INIT => X"FAFAFAFACCFFCC00"
    )
    port map (
      ADR1 => x_4_IBUF_0,
      ADR4 => x_cs(4),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out4,
      ADR5 => calc_sqr_root_calcPath_v,
      ADR0 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N94
    );
  corrNeg_cs_PWR_4_o_AND_42_o1 : X_LUT6
    generic map(
      LOC => "SLICE_X8Y10",
      INIT => X"000A000000000000"
    )
    port map (
      ADR1 => '1',
      ADR2 => counter_cs(0),
      ADR4 => counter_cs(2),
      ADR3 => counter_cs(1),
      ADR5 => counter_cs(3),
      ADR0 => corrNeg_cs_2011,
      O => corrNeg_cs_PWR_4_o_AND_42_o
    );
  counter_cs_2_2 : X_SFF
    generic map(
      LOC => "SLICE_X8Y10",
      INIT => '1'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_counter_cs_2_2_CLK,
      I => NlwBufferSignal_counter_cs_2_2_IN,
      O => counter_cs_2_2_1781,
      SSET => nRes_inv,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  counter_cs_2_1 : X_SFF
    generic map(
      LOC => "SLICE_X8Y10",
      INIT => '1'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_counter_cs_2_1_CLK,
      I => NlwBufferSignal_counter_cs_2_1_IN,
      O => counter_cs_2_1_1798,
      SSET => nRes_inv,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  Mmux_calc_sqr_root_n_v_3_0_11 : X_LUT6
    generic map(
      LOC => "SLICE_X8Y10",
      INIT => X"3FFF0000FFFF0000"
    )
    port map (
      ADR0 => '1',
      ADR2 => req_IBUF_0,
      ADR1 => rdy_cs_2_1794,
      ADR5 => counter_cs_2_1_1798,
      ADR4 => counter_cs_0_2_1779,
      ADR3 => counter_cs_3_1_1797,
      O => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q
    );
  rdy_cs_2 : X_SFF
    generic map(
      LOC => "SLICE_X8Y11",
      INIT => '1'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_rdy_cs_2_CLK,
      I => NlwBufferSignal_rdy_cs_2_IN,
      O => rdy_cs_2_1794,
      SSET => nRes_inv,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  rdy_cs_1 : X_SFF
    generic map(
      LOC => "SLICE_X8Y11",
      INIT => '1'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_rdy_cs_1_CLK,
      I => NlwBufferSignal_rdy_cs_1_IN,
      O => rdy_cs_1_1799,
      SSET => nRes_inv,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  N158_N158_DMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => N160,
      O => N160_0
    );
  Mmux_calc_sqr_root_corrNeg_v161_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y3",
      INIT => X"CCCCCCC8CCCCCCC8"
    )
    port map (
      ADR1 => x_11_IBUF_0,
      ADR2 => x_5_IBUF_0,
      ADR3 => x_4_IBUF_0,
      ADR0 => Mmux_calc_sqr_root_calcPath_v11_1915,
      ADR4 => Mmux_calc_sqr_root_calcPath_v1,
      ADR5 => '1',
      O => N158
    );
  Mmux_calc_sqr_root_approx_v11021_SW0 : X_LUT5
    generic map(
      LOC => "SLICE_X9Y3",
      INIT => X"CCCCCCCD"
    )
    port map (
      ADR1 => x_11_IBUF_0,
      ADR2 => x_5_IBUF_0,
      ADR3 => x_4_IBUF_0,
      ADR0 => Mmux_calc_sqr_root_calcPath_v11_1915,
      ADR4 => Mmux_calc_sqr_root_calcPath_v1,
      O => N160
    );
  Mmux_calc_sqr_root_calcPath_v11 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y3",
      INIT => X"FFFFFFFFFEFEFEFE"
    )
    port map (
      ADR4 => '1',
      ADR3 => '1',
      ADR2 => x_8_IBUF_0,
      ADR1 => x_9_IBUF_0,
      ADR0 => x_6_IBUF_0,
      ADR5 => x_7_IBUF_0,
      O => Mmux_calc_sqr_root_calcPath_v1
    );
  Mmux_calc_sqr_root_approx_v174_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y4",
      INIT => X"D1C01100C0C00000"
    )
    port map (
      ADR4 => N158,
      ADR2 => x_2_IBUF_0,
      ADR3 => x_cs(2),
      ADR0 => N176,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => N48
    );
  Mmux_calc_sqr_root_approx_v165_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y4",
      INIT => X"AAAA00000C000C00"
    )
    port map (
      ADR4 => N158,
      ADR0 => x_1_IBUF_0,
      ADR1 => x_cs(1),
      ADR2 => N176,
      ADR5 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => N46
    );
  Mmux_calc_sqr_root_approx_v11021 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y4",
      INIT => X"0033333300505050"
    )
    port map (
      ADR1 => N160_0,
      ADR0 => x_cs(11),
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR5 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => Mmux_calc_sqr_root_approx_v1102
    );
  Mmux_calc_sqr_root_approx_v1271 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y5",
      INIT => X"FCFF0C0FFCF00C00"
    )
    port map (
      ADR0 => '1',
      ADR1 => x_0_IBUF_0,
      ADR5 => x_cs(0),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => Mmux_calc_sqr_root_calcPath_v181,
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out,
      O => x_cs_11_GND_4_o_mux_26_OUT_0_Q
    );
  counter_cs_3_PWR_4_o_LessThan_16_o3 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y5",
      INIT => X"000C00CC000A00AA"
    )
    port map (
      ADR1 => x_0_IBUF_0,
      ADR0 => x_cs(0),
      ADR2 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR5 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => x_cs_11_x_11_mux_14_OUT_11_Q,
      O => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out
    );
  Mmux_calc_sqr_root_approx_v1281 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y5",
      INIT => X"08080C00AAAAFF00"
    )
    port map (
      ADR0 => x_10_IBUF_0,
      ADR3 => x_cs(10),
      ADR1 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => x_cs_11_x_11_mux_14_OUT_11_Q,
      ADR5 => Mmux_calc_sqr_root_calcPath_v18_1894,
      O => x_cs_11_GND_4_o_mux_26_OUT_10_Q
    );
  Mmux_calc_sqr_root_approx_v1311 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y5",
      INIT => X"303A3A3A000A0A0A"
    )
    port map (
      ADR5 => x_2_IBUF_0,
      ADR0 => x_cs(2),
      ADR2 => rdy_cs_PWR_4_o_AND_2_o,
      ADR1 => N217,
      ADR3 => N218,
      ADR4 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => x_cs_11_GND_4_o_mux_26_OUT_2_Q
    );
  x_cs_7 : X_SFF
    generic map(
      LOC => "SLICE_X9Y6",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_x_cs_7_CLK,
      I => calc_sqr_root_x_v(7),
      O => x_cs(7),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v1241 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y6",
      INIT => X"EEF322F3EEC022C0"
    )
    port map (
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_7_0,
      ADR1 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR4 => N109,
      ADR5 => N106,
      ADR0 => N107,
      ADR2 => N108,
      O => calc_sqr_root_x_v(7)
    );
  Mmux_calc_sqr_root_approx_v1241_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y6",
      INIT => X"0E0EFE0E0202F202"
    )
    port map (
      ADR5 => x_7_IBUF_0,
      ADR0 => x_cs(7),
      ADR1 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out7,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N108
    );
  x_cs_6 : X_SFF
    generic map(
      LOC => "SLICE_X9Y6",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_x_cs_6_CLK,
      I => calc_sqr_root_x_v(6),
      O => x_cs(6),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v1231 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y6",
      INIT => X"FF55D8D8AA00D8D8"
    )
    port map (
      ADR0 => GND_4_o_GND_4_o_sub_28_OUT_6_0,
      ADR4 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR3 => N104,
      ADR2 => N101,
      ADR1 => N102,
      ADR5 => N103,
      O => calc_sqr_root_x_v(6)
    );
  Mmux_calc_sqr_root_approx_v1231_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y6",
      INIT => X"3322F3E20022C0E2"
    )
    port map (
      ADR5 => x_6_IBUF_0,
      ADR0 => x_cs(6),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out6,
      ADR1 => calc_sqr_root_calcPath_v,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N103
    );
  Mmux_calc_sqr_root_approx_v13_Mmux_calc_sqr_root_approx_v13_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => GND_4_o_Decoder_30_OUT_4_3_11,
      O => GND_4_o_Decoder_30_OUT_4_3_11_0
    );
  Mmux_calc_sqr_root_approx_v131 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y7",
      INIT => X"0004CC8C0C040C8C"
    )
    port map (
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR2 => N200,
      ADR0 => Sh71,
      ADR5 => Sh91,
      ADR4 => N201,
      ADR1 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => Mmux_calc_sqr_root_approx_v13
    );
  GND_4_o_Decoder_30_OUT_10_3_111 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y7",
      INIT => X"CCCC0404CCCC0404"
    )
    port map (
      ADR3 => '1',
      ADR2 => counter_cs(2),
      ADR0 => counter_cs(3),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR1 => calc_sqr_root_calcPath_v,
      ADR5 => '1',
      O => GND_4_o_Decoder_30_OUT_10_3_11
    );
  GND_4_o_Decoder_30_OUT_4_3_111 : X_LUT5
    generic map(
      LOC => "SLICE_X9Y7",
      INIT => X"50005000"
    )
    port map (
      ADR3 => Mmux_calc_sqr_root_calcPath_v16_1850,
      ADR2 => counter_cs(2),
      ADR0 => counter_cs(3),
      ADR1 => '1',
      ADR4 => '1',
      O => GND_4_o_Decoder_30_OUT_4_3_11
    );
  Mmux_calc_sqr_root_calcPath_v17 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y7",
      INIT => X"FFFFFFFFFFFE0000"
    )
    port map (
      ADR1 => x_cs(5),
      ADR3 => x_cs(4),
      ADR2 => Mmux_calc_sqr_root_calcPath_v14_2017,
      ADR4 => Mmux_calc_sqr_root_calcPath_v13_2016,
      ADR5 => Mmux_calc_sqr_root_calcPath_v12_2010,
      ADR0 => Mmux_calc_sqr_root_calcPath_v15_2018,
      O => Mmux_calc_sqr_root_calcPath_v16_1850
    );
  Mmux_calc_sqr_root_calcPath_v18 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y7",
      INIT => X"FFFFFFFBCCCCCCC8"
    )
    port map (
      ADR3 => Mmux_calc_sqr_root_calcPath_v11_1915,
      ADR2 => x_5_IBUF_0,
      ADR0 => x_4_IBUF_0,
      ADR4 => Mmux_calc_sqr_root_calcPath_v1,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o1_2009,
      ADR5 => Mmux_calc_sqr_root_calcPath_v17_2022,
      O => calc_sqr_root_calcPath_v
    );
  N164_N164_AMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => rdy_cs_PWR_4_o_AND_2_o_mmx_out6,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out6_0
    );
  Mmux_calc_sqr_root_approx_v111_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y8",
      INIT => X"9393AFAF9933AAFF"
    )
    port map (
      ADR2 => x_6_IBUF_0,
      ADR3 => x_cs(6),
      ADR5 => rdy_cs_PWR_4_o_AND_2_o,
      ADR1 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out6,
      ADR4 => Mmux_calc_sqr_root_calcPath_v18_1894,
      ADR0 => Mmux_calc_sqr_root_approx_v1101,
      O => N164
    );
  Mmux_calc_sqr_root_calcPath_v15 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y8",
      INIT => X"FFFFFFFFFFFFFFF0"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR4 => x_cs(8),
      ADR5 => x_cs(9),
      ADR2 => x_cs(6),
      ADR3 => x_cs(7),
      O => Mmux_calc_sqr_root_calcPath_v14_2017
    );
  Mmux_calc_sqr_root_approx_v1221_SW3 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y8",
      INIT => X"FEFEAEAEFE54AE04"
    )
    port map (
      ADR4 => x_5_IBUF_0,
      ADR1 => x_cs(5),
      ADR2 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out5,
      ADR0 => calc_sqr_root_calcPath_v,
      ADR3 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N99
    );
  Mmux_x_cs_11_x_11_mux_14_OUT211 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y8",
      INIT => X"CCCCAAAACCCCAAAA"
    )
    port map (
      ADR2 => '1',
      ADR3 => '1',
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR0 => x_cs(6),
      ADR1 => x_6_IBUF_0,
      ADR5 => '1',
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out7
    );
  Mmux_x_cs_11_x_11_mux_14_OUT201 : X_LUT5
    generic map(
      LOC => "SLICE_X9Y8",
      INIT => X"FF00F0F0"
    )
    port map (
      ADR3 => x_5_IBUF_0,
      ADR2 => x_cs(5),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR0 => '1',
      ADR1 => '1',
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out6
    );
  Mmux_calc_sqr_root_approx_v163_Mmux_calc_sqr_root_approx_v163_BMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => N196,
      O => N196_0
    );
  Mmux_calc_sqr_root_approx_v164 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y9",
      INIT => X"0C2E0C0C00000000"
    )
    port map (
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR2 => rdy_cs_PWR_4_o_AND_2_o_mmx_out2,
      ADR0 => Sh9,
      ADR3 => x_cs_11_x_11_mux_14_OUT_11_Q,
      ADR5 => Mmux_calc_sqr_root_calcPath_v18_1894,
      O => Mmux_calc_sqr_root_approx_v163_1931
    );
  Mmux_x_cs_11_x_11_mux_14_OUT241 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y9",
      INIT => X"AAAAFF00AAAAFF00"
    )
    port map (
      ADR2 => '1',
      ADR1 => '1',
      ADR0 => x_11_IBUF_0,
      ADR3 => x_cs(11),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o11,
      ADR5 => '1',
      O => x_cs_11_x_11_mux_14_OUT_11_Q
    );
  Mmux_x_cs_11_x_11_mux_14_OUT231_SW0 : X_LUT5
    generic map(
      LOC => "SLICE_X9Y9",
      INIT => X"BBBBBBBB"
    )
    port map (
      ADR2 => '1',
      ADR1 => x_8_IBUF_0,
      ADR0 => x_11_IBUF_0,
      ADR3 => '1',
      ADR4 => '1',
      O => N196
    );
  rdy_cs_PWR_4_o_AND_2_o1_2 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y9",
      INIT => X"8000800080000000"
    )
    port map (
      ADR1 => req_IBUF_0,
      ADR5 => counter_cs_1_2_1778,
      ADR4 => counter_cs_0_2_1779,
      ADR2 => counter_cs_3_2_1780,
      ADR0 => counter_cs_2_2_1781,
      ADR3 => rdy_cs_1782,
      O => rdy_cs_PWR_4_o_AND_2_o11
    );
  Mmux_calc_sqr_root_approx_v1221_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y10",
      INIT => X"0F0F0000AF0FA000"
    )
    port map (
      ADR1 => '1',
      ADR0 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR4 => rdy_cs_PWR_4_o_AND_2_o_mmx_out6_0,
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out5,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR5 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N96
    );
  Mmux_calc_sqr_root_counter_v1_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X9Y10",
      INIT => X"3FFF00007FFF0000"
    )
    port map (
      ADR4 => rdy_cs_1782,
      ADR1 => counter_cs(2),
      ADR2 => counter_cs(3),
      ADR3 => req_IBUF_0,
      ADR5 => counter_cs(0),
      ADR0 => counter_cs(1),
      O => N40
    );
  N144_N144_AMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => N38_pack_9,
      O => N38
    );
  Mmux_calc_sqr_root_approx_v116_SW4 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y3",
      INIT => X"FCFCFC0CFAFAFA0A"
    )
    port map (
      ADR1 => x_10_IBUF_0,
      ADR0 => x_cs(10),
      ADR5 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out10,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N144
    );
  counter_cs_3_PWR_4_o_LessThan_16_o21 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y3",
      INIT => X"1100D1C000000000"
    )
    port map (
      ADR2 => x_10_IBUF_0,
      ADR4 => x_11_IBUF_0,
      ADR3 => x_cs(10),
      ADR0 => x_cs(11),
      ADR5 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o,
      O => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out10
    );
  Mmux_calc_sqr_root_approx_v116_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y3",
      INIT => X"3030000000FF00FF"
    )
    port map (
      ADR0 => '1',
      ADR2 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR3 => N38,
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out10,
      ADR5 => calc_sqr_root_calcPath_v,
      ADR1 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N141
    );
  Mmux_calc_sqr_root_approx_v143 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y3",
      INIT => X"CCFFCC00CCFFCC00"
    )
    port map (
      ADR0 => '1',
      ADR2 => '1',
      ADR1 => x_10_IBUF_0,
      ADR4 => x_cs(10),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => '1',
      O => Mmux_calc_sqr_root_approx_v141
    );
  Mmux_calc_sqr_root_approx_v116_SW0 : X_LUT5
    generic map(
      LOC => "SLICE_X10Y3",
      INIT => X"330033FF"
    )
    port map (
      ADR0 => '1',
      ADR2 => '1',
      ADR1 => x_10_IBUF_0,
      ADR4 => x_cs(10),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      O => N38_pack_9
    );
  Mmux_calc_sqr_root_approx_v184_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y4",
      INIT => X"AAAA0C0000000C00"
    )
    port map (
      ADR0 => N158,
      ADR5 => x_3_IBUF_0,
      ADR1 => x_cs(3),
      ADR2 => N176,
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => N50
    );
  Mmux_calc_sqr_root_corrNeg_v161_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y4",
      INIT => X"DD55DD55DD55DD55"
    )
    port map (
      ADR4 => '1',
      ADR5 => '1',
      ADR2 => '1',
      ADR0 => x_cs(11),
      ADR3 => counter_cs(2),
      ADR1 => counter_cs(3),
      O => N176
    );
  x_cs_11_GND_4_o_mux_26_OUT_3_x_cs_11_GND_4_o_mux_26_OUT_3_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => Mmux_calc_sqr_root_approx_v1132,
      O => Mmux_calc_sqr_root_approx_v1132_0
    );
  Mmux_calc_sqr_root_approx_v11321 : X_MUX2
    generic map(
      LOC => "SLICE_X10Y5"
    )
    port map (
      IA => N264,
      IB => N265,
      O => Mmux_calc_sqr_root_approx_v1132,
      SEL => rdy_cs_PWR_4_o_AND_2_o
    );
  Mmux_calc_sqr_root_approx_v11321_F : X_LUT6
    generic map(
      LOC => "SLICE_X10Y5",
      INIT => X"0000030300000000"
    )
    port map (
      ADR0 => '1',
      ADR3 => '1',
      ADR4 => x_cs(11),
      ADR1 => counter_cs(2),
      ADR2 => counter_cs(3),
      ADR5 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => N264
    );
  Mmux_calc_sqr_root_approx_v11321_G : X_LUT6
    generic map(
      LOC => "SLICE_X10Y5",
      INIT => X"00000000FFFFFEFE"
    )
    port map (
      ADR3 => '1',
      ADR5 => x_11_IBUF_0,
      ADR2 => x_4_IBUF_0,
      ADR1 => x_5_IBUF_0,
      ADR0 => Mmux_calc_sqr_root_calcPath_v11_1915,
      ADR4 => Mmux_calc_sqr_root_calcPath_v1,
      O => N265
    );
  Mmux_calc_sqr_root_approx_v1321 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y5",
      INIT => X"4777033344440000"
    )
    port map (
      ADR4 => x_3_IBUF_0,
      ADR5 => x_cs(3),
      ADR1 => rdy_cs_PWR_4_o_AND_2_o,
      ADR0 => N217,
      ADR2 => N218,
      ADR3 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => x_cs_11_GND_4_o_mux_26_OUT_3_Q
    );
  Mmux_calc_sqr_root_approx_v1301_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y6",
      INIT => X"CCCCFFFFCCC8FFFA"
    )
    port map (
      ADR1 => x_11_IBUF_0,
      ADR2 => Mmux_calc_sqr_root_calcPath_v1,
      ADR5 => x_5_IBUF_0,
      ADR0 => x_4_IBUF_0,
      ADR3 => Mmux_calc_sqr_root_calcPath_v11_1915,
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o,
      O => N217
    );
  counter_cs_3_PWR_4_o_LessThan_16_o111 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y6",
      INIT => X"0000227222722272"
    )
    port map (
      ADR1 => N198_0,
      ADR2 => x_cs(9),
      ADR3 => x_cs(11),
      ADR5 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      O => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out9
    );
  Mmux_calc_sqr_root_approx_v1381 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y6",
      INIT => X"FFDD2705FAD82200"
    )
    port map (
      ADR3 => x_9_IBUF_0,
      ADR1 => N209_0,
      ADR5 => x_cs(9),
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => Mmux_calc_sqr_root_calcPath_v16_1850,
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out9,
      O => x_cs_11_GND_4_o_mux_26_OUT_9_Q
    );
  N188_N188_DMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => N209,
      O => N209_0
    );
  N188_N188_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => rdy_cs_PWR_4_o_AND_2_o_mmx_out10,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out10_0
    );
  Mmux_x_cs_11_x_11_mux_14_OUT191_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y7",
      INIT => X"FFFF3333FFFF3333"
    )
    port map (
      ADR0 => '1',
      ADR3 => '1',
      ADR2 => '1',
      ADR1 => x_4_IBUF_0,
      ADR4 => x_11_IBUF_0,
      ADR5 => '1',
      O => N188
    );
  Mmux_calc_sqr_root_calcPath_v18_SW5 : X_LUT5
    generic map(
      LOC => "SLICE_X10Y7",
      INIT => X"FFFEFFFE"
    )
    port map (
      ADR2 => x_5_IBUF_0,
      ADR3 => Mmux_calc_sqr_root_calcPath_v11_1915,
      ADR0 => Mmux_calc_sqr_root_calcPath_v1,
      ADR1 => x_4_IBUF_0,
      ADR4 => '1',
      O => N209
    );
  Mmux_x_cs_11_x_11_mux_14_OUT71 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y7",
      INIT => X"FFFF00FFFFFF00FF"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR2 => '1',
      ADR3 => approx_cs(4),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => '1',
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out14
    );
  Mmux_x_cs_11_x_11_mux_14_OUT31 : X_LUT5
    generic map(
      LOC => "SLICE_X10Y7",
      INIT => X"F0F0CCCC"
    )
    port map (
      ADR0 => '1',
      ADR2 => x_9_IBUF_0,
      ADR1 => x_cs(9),
      ADR3 => '1',
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out10
    );
  counter_cs_3_PWR_4_o_LessThan_16_o121 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y7",
      INIT => X"8880FFFFFFFFFFFF"
    )
    port map (
      ADR1 => req_IBUF_0,
      ADR5 => counter_cs_3_2_1780,
      ADR4 => counter_cs_2_2_1781,
      ADR3 => counter_cs_1_2_1778,
      ADR2 => counter_cs_0_2_1779,
      ADR0 => rdy_cs_1782,
      O => counter_cs_3_PWR_4_o_LessThan_16_o
    );
  Mmux_calc_sqr_root_approx_v131_SW2_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y7",
      INIT => X"FFFFFFFF3FAF3FFF"
    )
    port map (
      ADR1 => approx_cs(0),
      ADR0 => x_cs(11),
      ADR3 => counter_cs(2),
      ADR2 => counter_cs(3),
      ADR5 => rdy_cs_PWR_4_o_AND_2_o11,
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o,
      O => N201
    );
  counter_cs_3_PWR_4_o_LessThan_16_o61 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y8",
      INIT => X"0004000CFF04FF0C"
    )
    port map (
      ADR5 => N188,
      ADR1 => x_cs(4),
      ADR2 => x_cs(11),
      ADR4 => counter_cs(3),
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      O => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out4
    );
  Mmux_calc_sqr_root_approx_v1331 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y8",
      INIT => X"EFEFEFE0000F0000"
    )
    port map (
      ADR0 => x_5_IBUF_0,
      ADR1 => N203,
      ADR4 => x_cs(4),
      ADR2 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => Mmux_calc_sqr_root_calcPath_v16_1850,
      ADR5 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out4,
      O => x_cs_11_GND_4_o_mux_26_OUT_4_Q
    );
  Mmux_calc_sqr_root_n_v_3_0_31 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y8",
      INIT => X"5F7FFFFF00000000"
    )
    port map (
      ADR0 => req_IBUF_0,
      ADR4 => rdy_cs_2_1794,
      ADR1 => counter_cs_1_1_1795,
      ADR3 => counter_cs_0_1_1796,
      ADR2 => counter_cs_3_1_1797,
      ADR5 => counter_cs_2_1_1798,
      O => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q
    );
  counter_cs_3_PWR_4_o_LessThan_16_o81 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y8",
      INIT => X"05050F0F11003300"
    )
    port map (
      ADR2 => N192,
      ADR3 => x_cs(6),
      ADR1 => x_cs(11),
      ADR5 => rdy_cs_PWR_4_o_AND_2_o11,
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      O => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out6
    );
  corrNeg_cs_counter_cs_3_AND_41_o1 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y9",
      INIT => X"0001000100000000"
    )
    port map (
      ADR4 => '1',
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR2 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR5 => calc_sqr_root_corrNeg_v,
      O => corrNeg_cs_counter_cs_3_AND_41_o
    );
  corrNeg_cs : X_SFF
    generic map(
      LOC => "SLICE_X10Y9",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_corrNeg_cs_CLK,
      I => calc_sqr_root_corrNeg_v,
      O => corrNeg_cs_2011,
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_corrNeg_v1 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y9",
      INIT => X"FFFFBFFFFFFF0000"
    )
    port map (
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => counter_cs(3),
      ADR3 => counter_cs(2),
      ADR5 => corrNeg_cs_2011,
      ADR1 => N18,
      ADR4 => Mmux_calc_sqr_root_approx_v1101,
      O => calc_sqr_root_corrNeg_v
    );
  Mmux_calc_sqr_root_calcPath_v14 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y10",
      INIT => X"00FFFFFF00FFFFF0"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR5 => counter_cs_0_1_1796,
      ADR2 => counter_cs_1_1_1795,
      ADR4 => counter_cs_2_1_1798,
      ADR3 => counter_cs_3_1_1797,
      O => Mmux_calc_sqr_root_calcPath_v13_2016
    );
  rdy_cs : X_SFF
    generic map(
      LOC => "SLICE_X10Y10",
      INIT => '1'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_rdy_cs_CLK,
      I => calc_sqr_root_rdy_v,
      O => rdy_cs_1782,
      SSET => nRes_inv,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  Mmux_calc_sqr_root_rdy_v11 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y10",
      INIT => X"AF0FAA008F0F8800"
    )
    port map (
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR4 => rdy_cs_1782,
      ADR5 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR2 => rdy_cs_PWR_4_o_AND_2_o,
      O => calc_sqr_root_rdy_v
    );
  counter_cs_0_2 : X_SFF
    generic map(
      LOC => "SLICE_X10Y10",
      INIT => '1'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_counter_cs_0_2_CLK,
      I => calc_sqr_root_counter_v(0),
      O => counter_cs_0_2_1779,
      SSET => nRes_inv,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  Mmux_calc_sqr_root_counter_v1 : X_LUT6
    generic map(
      LOC => "SLICE_X10Y10",
      INIT => X"F1F5F5F501050505"
    )
    port map (
      ADR2 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR0 => N40,
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR4 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR5 => calc_sqr_root_rdy_v,
      O => calc_sqr_root_counter_v(0)
    );
  counter_cs_0_1 : X_SFF
    generic map(
      LOC => "SLICE_X10Y10",
      INIT => '1'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_counter_cs_0_1_CLK,
      I => NlwBufferSignal_counter_cs_0_1_IN,
      O => counter_cs_0_1_1796,
      SSET => nRes_inv,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  Mmux_calc_sqr_root_approx_v116_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y3",
      INIT => X"FCFC5555FFFF5555"
    )
    port map (
      ADR3 => '1',
      ADR5 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR0 => N38,
      ADR1 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out10,
      ADR4 => calc_sqr_root_calcPath_v,
      ADR2 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N142
    );
  x_cs_11 : X_SFF
    generic map(
      LOC => "SLICE_X11Y3",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_x_cs_11_CLK,
      I => calc_sqr_root_x_v(11),
      O => x_cs(11),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v1171 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y3",
      INIT => X"FE32DC1032321010"
    )
    port map (
      ADR4 => x_11_IBUF_0,
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR1 => calc_sqr_root_calcPath_v,
      ADR2 => x_cs(11),
      ADR3 => corrNeg_cs_counter_cs_3_AND_41_o,
      ADR5 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      O => calc_sqr_root_x_v(11)
    );
  x_cs_10 : X_SFF
    generic map(
      LOC => "SLICE_X11Y3",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_x_cs_10_CLK,
      I => calc_sqr_root_x_v(10),
      O => x_cs(10),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v116 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y3",
      INIT => X"F0CCFFAAF0CC00AA"
    )
    port map (
      ADR4 => GND_4_o_GND_4_o_sub_28_OUT_10_0,
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR2 => N144,
      ADR0 => N141,
      ADR1 => N142,
      ADR5 => N143,
      O => calc_sqr_root_x_v(10)
    );
  Mmux_calc_sqr_root_approx_v116_SW3 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y3",
      INIT => X"00FF0000CACACACA"
    )
    port map (
      ADR1 => x_10_IBUF_0,
      ADR0 => x_cs(10),
      ADR2 => rdy_cs_PWR_4_o_AND_2_o,
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out10,
      ADR5 => calc_sqr_root_calcPath_v,
      ADR3 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N143
    );
  Mmux_x_cs_11_x_11_mux_14_OUT162 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y4",
      INIT => X"0000000000000001"
    )
    port map (
      ADR1 => x_6_IBUF_0,
      ADR3 => x_5_IBUF_0,
      ADR2 => x_7_IBUF_0,
      ADR4 => x_8_IBUF_0,
      ADR0 => x_9_IBUF_0,
      ADR5 => x_10_IBUF_0,
      O => Mmux_x_cs_11_x_11_mux_14_OUT161_1903
    );
  Mmux_calc_sqr_root_calcPath_v16 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y5",
      INIT => X"FFFFFFFFFFFFFFFA"
    )
    port map (
      ADR1 => '1',
      ADR5 => x_cs(0),
      ADR0 => x_cs(1),
      ADR3 => x_cs(10),
      ADR4 => x_cs(3),
      ADR2 => x_cs(2),
      O => Mmux_calc_sqr_root_calcPath_v15_2018
    );
  x_cs_1 : X_SFF
    generic map(
      LOC => "SLICE_X11Y5",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_x_cs_1_CLK,
      I => calc_sqr_root_x_v(1),
      O => x_cs(1),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v118 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y5",
      INIT => X"E2FFE2CCE233E200"
    )
    port map (
      ADR1 => GND_4_o_GND_4_o_sub_28_OUT_1_0,
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR2 => N139,
      ADR4 => N136,
      ADR5 => N137,
      ADR0 => N138,
      O => calc_sqr_root_x_v(1)
    );
  Mmux_calc_sqr_root_approx_v118_SW3 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y5",
      INIT => X"0F0F0000AACCAACC"
    )
    port map (
      ADR0 => x_1_IBUF_0,
      ADR1 => x_cs(1),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out1,
      ADR5 => calc_sqr_root_calcPath_v,
      ADR2 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N138
    );
  x_cs_0 : X_SFF
    generic map(
      LOC => "SLICE_X11Y5",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_x_cs_0_CLK,
      I => calc_sqr_root_x_v(0),
      O => x_cs(0),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v115 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y5",
      INIT => X"FF00FF00AAAABB88"
    )
    port map (
      ADR2 => '1',
      ADR1 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR0 => N150_0,
      ADR5 => corrNeg_cs_counter_cs_3_AND_41_o,
      ADR3 => N149,
      ADR4 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      O => calc_sqr_root_x_v(0)
    );
  N104_N104_CMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => N198,
      O => N198_0
    );
  Mmux_calc_sqr_root_approx_v1231_SW3 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y6",
      INIT => X"FBFBFBC8C8FBC8C8"
    )
    port map (
      ADR5 => x_6_IBUF_0,
      ADR4 => x_cs(6),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out6,
      ADR1 => calc_sqr_root_calcPath_v,
      ADR0 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N104
    );
  Mmux_x_cs_11_x_11_mux_14_OUT201_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y6",
      INIT => X"F5F5F5F5F5F5F5F5"
    )
    port map (
      ADR3 => '1',
      ADR1 => '1',
      ADR4 => '1',
      ADR0 => x_5_IBUF_0,
      ADR2 => x_11_IBUF_0,
      ADR5 => '1',
      O => N190
    );
  Mmux_x_cs_11_x_11_mux_14_OUT31_SW0 : X_LUT5
    generic map(
      LOC => "SLICE_X11Y6",
      INIT => X"F0FFF0FF"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR3 => x_9_IBUF_0,
      ADR4 => '1',
      ADR2 => x_11_IBUF_0,
      O => N198
    );
  Mmux_calc_sqr_root_approx_v111_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y6",
      INIT => X"FDFDFDA820207520"
    )
    port map (
      ADR2 => x_6_IBUF_0,
      ADR1 => N209_0,
      ADR3 => x_cs(6),
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR4 => Mmux_calc_sqr_root_calcPath_v16_1850,
      ADR5 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out6,
      O => N165
    );
  N107_N107_AMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => rdy_cs_PWR_4_o_AND_2_o_mmx_out8_pack_5,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out8
    );
  Mmux_calc_sqr_root_approx_v1241_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y7",
      INIT => X"FFFFFF0FCCCCCCCC"
    )
    port map (
      ADR0 => '1',
      ADR2 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o_mmx_out8,
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out7,
      ADR5 => calc_sqr_root_calcPath_v,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N107
    );
  Mmux_calc_sqr_root_approx_v1241_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y7",
      INIT => X"00CCAACC00CC00CC"
    )
    port map (
      ADR2 => '1',
      ADR0 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o_mmx_out8,
      ADR5 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out7,
      ADR3 => calc_sqr_root_calcPath_v,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N106
    );
  Mmux_x_cs_11_x_11_mux_14_OUT231 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y7",
      INIT => X"AAFFAA00AAFFAA00"
    )
    port map (
      ADR2 => '1',
      ADR1 => '1',
      ADR0 => x_8_IBUF_0,
      ADR4 => x_cs(8),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => '1',
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out9
    );
  Mmux_x_cs_11_x_11_mux_14_OUT221 : X_LUT5
    generic map(
      LOC => "SLICE_X11Y7",
      INIT => X"CCF0CCF0"
    )
    port map (
      ADR2 => x_cs(7),
      ADR1 => x_7_IBUF_0,
      ADR0 => '1',
      ADR4 => '1',
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      O => rdy_cs_PWR_4_o_AND_2_o_mmx_out8_pack_5
    );
  Mmux_calc_sqr_root_approx_v1231_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y8",
      INIT => X"0CAC0CAC0C0C0C0C"
    )
    port map (
      ADR4 => '1',
      ADR5 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o_mmx_out7,
      ADR0 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out6,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR3 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N101
    );
  Mmux_calc_sqr_root_approx_v1221_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y8",
      INIT => X"FAFAFA0AFAFAFAFA"
    )
    port map (
      ADR1 => '1',
      ADR5 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR0 => rdy_cs_PWR_4_o_AND_2_o_mmx_out6_0,
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out5,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR3 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N97
    );
  Mmux_calc_sqr_root_corrNeg_v161 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y8",
      INIT => X"FF2A002AFF000000"
    )
    port map (
      ADR4 => N158,
      ADR0 => x_cs(11),
      ADR2 => counter_cs(2),
      ADR1 => counter_cs(3),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o1_2009,
      ADR5 => Mmux_calc_sqr_root_calcPath_v17_2022,
      O => Mmux_calc_sqr_root_approx_v1101
    );
  Mmux_calc_sqr_root_calcPath_v17_1 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y8",
      INIT => X"FFF0FFF0FFF0FFE0"
    )
    port map (
      ADR1 => x_cs(5),
      ADR0 => x_cs(4),
      ADR4 => Mmux_calc_sqr_root_calcPath_v14_2017,
      ADR2 => Mmux_calc_sqr_root_calcPath_v13_2016,
      ADR3 => Mmux_calc_sqr_root_calcPath_v12_2010,
      ADR5 => Mmux_calc_sqr_root_calcPath_v15_2018,
      O => Mmux_calc_sqr_root_calcPath_v17_2022
    );
  counter_cs_1_2 : X_SFF
    generic map(
      LOC => "SLICE_X11Y9",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_counter_cs_1_2_CLK,
      I => NlwBufferSignal_counter_cs_1_2_IN,
      O => counter_cs_1_2_1778,
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  counter_cs_1_1 : X_SFF
    generic map(
      LOC => "SLICE_X11Y9",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_counter_cs_1_1_CLK,
      I => NlwBufferSignal_counter_cs_1_1_IN,
      O => counter_cs_1_1_1795,
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_calcPath_v13 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y9",
      INIT => X"333FFFFF00000000"
    )
    port map (
      ADR0 => '1',
      ADR5 => corrNeg_cs_2011,
      ADR3 => counter_cs_0_1_1796,
      ADR2 => counter_cs_1_1_1795,
      ADR4 => counter_cs_2_1_1798,
      ADR1 => counter_cs_3_1_1797,
      O => Mmux_calc_sqr_root_calcPath_v12_2010
    );
  counter_cs_3_2 : X_SFF
    generic map(
      LOC => "SLICE_X11Y10",
      INIT => '1'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_counter_cs_3_2_CLK,
      I => calc_sqr_root_counter_v(3),
      O => counter_cs_3_2_1780,
      SSET => nRes_inv,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  Mmux_calc_sqr_root_counter_v41 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y10",
      INIT => X"FF80FF88FF00FF00"
    )
    port map (
      ADR5 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR2 => rdy_cs_PWR_4_o_AND_2_o,
      ADR4 => rdy_cs_1782,
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      O => calc_sqr_root_counter_v(3)
    );
  counter_cs_3_1 : X_SFF
    generic map(
      LOC => "SLICE_X11Y10",
      INIT => '1'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_counter_cs_3_1_CLK,
      I => NlwBufferSignal_counter_cs_3_1_IN,
      O => counter_cs_3_1_1797,
      SSET => nRes_inv,
      SET => GND,
      RST => GND,
      SRST => GND
    );
  Mmux_calc_sqr_root_n_v_3_0_41 : X_LUT6
    generic map(
      LOC => "SLICE_X11Y10",
      INIT => X"57FF0000FFFF0000"
    )
    port map (
      ADR5 => req_IBUF_0,
      ADR3 => rdy_cs_1_1799,
      ADR1 => counter_cs_0_1_1796,
      ADR4 => counter_cs_3_1_1797,
      ADR2 => counter_cs_1_1_1795,
      ADR0 => counter_cs_2_1_1798,
      O => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q
    );
  Mmux_calc_sqr_root_approx_v120_SW4 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y3",
      INIT => X"FFF5CFC5FAF0CAC0"
    )
    port map (
      ADR3 => x_3_IBUF_0,
      ADR5 => x_cs(3),
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out3,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR1 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N134
    );
  Mmux_calc_sqr_root_approx_v120_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y3",
      INIT => X"F0A0FFAFF0F0FFFF"
    )
    port map (
      ADR1 => '1',
      ADR5 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR4 => N34,
      ADR0 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out3,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR3 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N132
    );
  Mmux_calc_sqr_root_approx_v120_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y3",
      INIT => X"0000F00033333333"
    )
    port map (
      ADR0 => '1',
      ADR2 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR1 => N34,
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out3,
      ADR5 => calc_sqr_root_calcPath_v,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N131
    );
  counter_cs_3_PWR_4_o_LessThan_16_o51 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y3",
      INIT => X"22000F0022000000"
    )
    port map (
      ADR0 => x_3_IBUF_0,
      ADR1 => x_11_IBUF_0,
      ADR5 => x_cs(3),
      ADR2 => x_cs(11),
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      O => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out3
    );
  x_cs_3 : X_SFF
    generic map(
      LOC => "SLICE_X12Y4",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_x_cs_3_CLK,
      I => calc_sqr_root_x_v(3),
      O => x_cs(3),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v120 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y4",
      INIT => X"FFD8AAD855D800D8"
    )
    port map (
      ADR0 => GND_4_o_GND_4_o_sub_28_OUT_3_0,
      ADR3 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR5 => N134,
      ADR2 => N131,
      ADR1 => N132,
      ADR4 => N133,
      O => calc_sqr_root_x_v(3)
    );
  Mmux_calc_sqr_root_approx_v120_SW3 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y4",
      INIT => X"55F544E400A044E4"
    )
    port map (
      ADR5 => x_3_IBUF_0,
      ADR1 => x_cs(3),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out3,
      ADR0 => calc_sqr_root_calcPath_v,
      ADR3 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N133
    );
  x_cs_2 : X_SFF
    generic map(
      LOC => "SLICE_X12Y4",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_x_cs_2_CLK,
      I => calc_sqr_root_x_v(2),
      O => x_cs(2),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v119 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y4",
      INIT => X"AFA0CFCFAFA0C0C0"
    )
    port map (
      ADR2 => GND_4_o_GND_4_o_sub_28_OUT_2_0,
      ADR4 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR0 => N129,
      ADR5 => N126,
      ADR1 => N127,
      ADR3 => N128,
      O => calc_sqr_root_x_v(2)
    );
  Mmux_calc_sqr_root_approx_v119_SW3 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y4",
      INIT => X"5044FAEE50445044"
    )
    port map (
      ADR2 => x_2_IBUF_0,
      ADR1 => x_cs(2),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out2,
      ADR0 => calc_sqr_root_calcPath_v,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N128
    );
  N44_N44_DMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => N150,
      O => N150_0
    );
  Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_lut_0_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y5",
      INIT => X"2727272727272727"
    )
    port map (
      ADR4 => '1',
      ADR3 => '1',
      ADR1 => x_0_IBUF_0,
      ADR2 => x_cs(0),
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => '1',
      O => N44
    );
  Mmux_calc_sqr_root_approx_v115_SW6 : X_LUT5
    generic map(
      LOC => "SLICE_X12Y5",
      INIT => X"FFD800D8"
    )
    port map (
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out,
      ADR3 => calc_sqr_root_calcPath_v,
      ADR1 => x_0_IBUF_0,
      ADR2 => x_cs(0),
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      O => N150
    );
  Mmux_calc_sqr_root_approx_v119_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y5",
      INIT => X"05AF050505050505"
    )
    port map (
      ADR1 => '1',
      ADR5 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR2 => N32,
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out2,
      ADR0 => calc_sqr_root_calcPath_v,
      ADR3 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N126
    );
  Mmux_calc_sqr_root_approx_v11311 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y5",
      INIT => X"0000C0000000C000"
    )
    port map (
      ADR0 => '1',
      ADR5 => '1',
      ADR3 => counter_cs(2),
      ADR1 => counter_cs(3),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR2 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => Mmux_calc_sqr_root_approx_v1131
    );
  counter_cs_3_PWR_4_o_LessThan_16_o71 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y6",
      INIT => X"2222277722222222"
    )
    port map (
      ADR1 => N190,
      ADR5 => x_cs(5),
      ADR4 => x_cs(11),
      ADR2 => counter_cs(3),
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      O => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out5
    );
  Mmux_calc_sqr_root_approx_v1341 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y6",
      INIT => X"FAFFFACC000000CC"
    )
    port map (
      ADR0 => x_5_IBUF_0,
      ADR2 => N203,
      ADR1 => x_cs(5),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR4 => Mmux_calc_sqr_root_calcPath_v16_1850,
      ADR5 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out5,
      O => x_cs_11_GND_4_o_mux_26_OUT_5_Q
    );
  Mmux_calc_sqr_root_approx_v110_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y6",
      INIT => X"0000800000008000"
    )
    port map (
      ADR5 => '1',
      ADR1 => approx_cs(5),
      ADR0 => counter_cs(2),
      ADR2 => counter_cs(3),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => Mmux_calc_sqr_root_calcPath_v16_1850,
      O => N54
    );
  Mmux_calc_sqr_root_approx_v1251_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y7",
      INIT => X"5F0F50000F0F0000"
    )
    port map (
      ADR1 => '1',
      ADR5 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR4 => rdy_cs_PWR_4_o_AND_2_o_mmx_out9,
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out8,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR0 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N111
    );
  rdy_cs_PWR_4_o_AND_2_o1 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y7",
      INIT => X"A080000000000000"
    )
    port map (
      ADR0 => req_IBUF_0,
      ADR4 => rdy_cs_1_1799,
      ADR3 => counter_cs_0_1_1796,
      ADR1 => counter_cs_1_1_1795,
      ADR5 => counter_cs_2_1_1798,
      ADR2 => counter_cs_3_1_1797,
      O => rdy_cs_PWR_4_o_AND_2_o
    );
  Mmux_calc_sqr_root_approx_v115_SW5 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y7",
      INIT => X"AAAAAAAAFF00CCCC"
    )
    port map (
      ADR2 => '1',
      ADR5 => calc_sqr_root_calcPath_v,
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => x_0_IBUF_0,
      ADR1 => x_cs(0),
      ADR0 => GND_4_o_GND_4_o_sub_28_OUT_0_0,
      O => N149
    );
  x_cs_9 : X_SFF
    generic map(
      LOC => "SLICE_X12Y8",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_x_cs_9_CLK,
      I => calc_sqr_root_x_v(9),
      O => x_cs(9),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v1261 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y8",
      INIT => X"CCCCFF00F0F0AAAA"
    )
    port map (
      ADR4 => GND_4_o_GND_4_o_sub_28_OUT_9_0,
      ADR5 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR1 => N119,
      ADR0 => N116,
      ADR2 => N117,
      ADR3 => N118,
      O => calc_sqr_root_x_v(9)
    );
  Mmux_calc_sqr_root_approx_v1261_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y8",
      INIT => X"0F0C000CAFACA0AC"
    )
    port map (
      ADR4 => x_9_IBUF_0,
      ADR1 => x_cs(9),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR0 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out9,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR5 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N118
    );
  x_cs_8 : X_SFF
    generic map(
      LOC => "SLICE_X12Y8",
      INIT => '0'
    )
    port map (
      CE => VCC,
      CLK => NlwBufferSignal_x_cs_8_CLK,
      I => calc_sqr_root_x_v(8),
      O => x_cs(8),
      SRST => nRes_inv,
      SET => GND,
      RST => GND,
      SSET => GND
    );
  Mmux_calc_sqr_root_approx_v1251 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y8",
      INIT => X"FF0FCACAF000CACA"
    )
    port map (
      ADR2 => GND_4_o_GND_4_o_sub_28_OUT_8_0,
      ADR4 => GND_4_o_GND_4_o_sub_28_OUT_11_0,
      ADR3 => N114,
      ADR0 => N111,
      ADR1 => N112,
      ADR5 => N113,
      O => calc_sqr_root_x_v(8)
    );
  Mmux_calc_sqr_root_approx_v1251_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X12Y8",
      INIT => X"00F000CCAAF0AACC"
    )
    port map (
      ADR2 => x_8_IBUF_0,
      ADR1 => x_cs(8),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR0 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out8,
      ADR3 => calc_sqr_root_calcPath_v,
      ADR5 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N113
    );
  Mmux_x_cs_11_x_11_mux_14_OUT161 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y1",
      INIT => X"0000000000000002"
    )
    port map (
      ADR2 => x_0_IBUF_0,
      ADR4 => x_1_IBUF_0,
      ADR3 => x_2_IBUF_0,
      ADR5 => x_3_IBUF_0,
      ADR0 => x_11_IBUF_0,
      ADR1 => x_4_IBUF_0,
      O => Mmux_x_cs_11_x_11_mux_14_OUT16
    );
  Mmux_calc_sqr_root_calcPath_v12 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y2",
      INIT => X"FFFFFFFFFFFFFEFE"
    )
    port map (
      ADR3 => '1',
      ADR5 => x_2_IBUF_0,
      ADR2 => x_3_IBUF_0,
      ADR0 => x_0_IBUF_0,
      ADR1 => x_10_IBUF_0,
      ADR4 => x_1_IBUF_0,
      O => Mmux_calc_sqr_root_calcPath_v11_1915
    );
  Mmux_calc_sqr_root_calcPath_v18_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y2",
      INIT => X"FFFFFFFFFFFFFFFE"
    )
    port map (
      ADR5 => x_4_IBUF_0,
      ADR4 => Mmux_calc_sqr_root_calcPath_v11_1915,
      ADR3 => x_8_IBUF_0,
      ADR1 => x_9_IBUF_0,
      ADR0 => x_6_IBUF_0,
      ADR2 => x_7_IBUF_0,
      O => N203
    );
  Mmux_calc_sqr_root_approx_v120_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y3",
      INIT => X"00005555AAAAFFFF"
    )
    port map (
      ADR3 => '1',
      ADR1 => '1',
      ADR2 => '1',
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => x_3_IBUF_0,
      ADR4 => x_cs(3),
      O => N34
    );
  Mmux_calc_sqr_root_approx_v119_SW4 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y4",
      INIT => X"FFFFEE44F0F0EE44"
    )
    port map (
      ADR3 => x_2_IBUF_0,
      ADR1 => x_cs(2),
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out2,
      ADR4 => calc_sqr_root_calcPath_v,
      ADR2 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N129
    );
  counter_cs_3_PWR_4_o_LessThan_16_o41 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y4",
      INIT => X"000A0000808A8080"
    )
    port map (
      ADR1 => x_2_IBUF_0,
      ADR5 => x_11_IBUF_0,
      ADR4 => x_cs(2),
      ADR3 => x_cs(11),
      ADR0 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR2 => rdy_cs_PWR_4_o_AND_2_o,
      O => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out2
    );
  N127_N127_AMUX_Delay : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => N36_pack_8,
      O => N36
    );
  Mmux_calc_sqr_root_approx_v119_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y5",
      INIT => X"F0C0FFCFF0F0FFFF"
    )
    port map (
      ADR0 => '1',
      ADR5 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR4 => N32,
      ADR1 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out2,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR3 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N127
    );
  Mmux_calc_sqr_root_approx_v118_SW2 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y5",
      INIT => X"F3F3F353F3F3F353"
    )
    port map (
      ADR5 => '1',
      ADR0 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR1 => N36,
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out1,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N137
    );
  Mmux_calc_sqr_root_approx_v118_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y5",
      INIT => X"0055005580D580D5"
    )
    port map (
      ADR4 => '1',
      ADR2 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR3 => N36,
      ADR1 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out1,
      ADR0 => calc_sqr_root_calcPath_v,
      ADR5 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N136
    );
  Mmux_calc_sqr_root_approx_v119_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y5",
      INIT => X"00FF555500FF5555"
    )
    port map (
      ADR2 => '1',
      ADR1 => '1',
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => x_2_IBUF_0,
      ADR0 => x_cs(2),
      ADR5 => '1',
      O => N32
    );
  Mmux_calc_sqr_root_approx_v118_SW0 : X_LUT5
    generic map(
      LOC => "SLICE_X13Y5",
      INIT => X"33330F0F"
    )
    port map (
      ADR1 => x_1_IBUF_0,
      ADR2 => x_cs(1),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR3 => '1',
      ADR0 => '1',
      O => N36_pack_8
    );
  counter_cs_3_PWR_4_o_LessThan_16_o11 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y6",
      INIT => X"11000000D100C000"
    )
    port map (
      ADR2 => x_1_IBUF_0,
      ADR5 => x_11_IBUF_0,
      ADR4 => x_cs(1),
      ADR0 => x_cs(11),
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o,
      ADR1 => rdy_cs_PWR_4_o_AND_2_o,
      O => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out1
    );
  Mmux_calc_sqr_root_approx_v118_SW4 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y6",
      INIT => X"FFEEF3E2DDCCD1C0"
    )
    port map (
      ADR5 => x_1_IBUF_0,
      ADR3 => x_cs(1),
      ADR0 => rdy_cs_PWR_4_o_AND_2_o,
      ADR4 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out1,
      ADR1 => calc_sqr_root_calcPath_v,
      ADR2 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N139
    );
  Mmux_calc_sqr_root_approx_v1261_SW0 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y7",
      INIT => X"3F0F0F0F30000000"
    )
    port map (
      ADR0 => '1',
      ADR4 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR5 => rdy_cs_PWR_4_o_AND_2_o_mmx_out10_0,
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out9,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR1 => corrNeg_cs_counter_cs_3_AND_41_o,
      O => N116
    );
  Mmux_calc_sqr_root_approx_v1251_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y7",
      INIT => X"FFCFF0C0FFFFF0F0"
    )
    port map (
      ADR0 => '1',
      ADR5 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR4 => rdy_cs_PWR_4_o_AND_2_o_mmx_out9,
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out8,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR1 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N112
    );
  counter_cs_3_PWR_4_o_LessThan_16_o101 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y7",
      INIT => X"1515000015153F00"
    )
    port map (
      ADR0 => N196_0,
      ADR3 => x_cs(8),
      ADR5 => x_cs(11),
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR2 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      O => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out8
    );
  Mmux_calc_sqr_root_approx_v1371 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y7",
      INIT => X"FF00FC30BB88B8B8"
    )
    port map (
      ADR0 => x_8_IBUF_0,
      ADR5 => N209_0,
      ADR2 => x_cs(8),
      ADR1 => rdy_cs_PWR_4_o_AND_2_o,
      ADR4 => Mmux_calc_sqr_root_calcPath_v16_1850,
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out8,
      O => x_cs_11_GND_4_o_mux_26_OUT_8_Q
    );
  Mmux_calc_sqr_root_approx_v1251_SW3 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y8",
      INIT => X"FFFCAFACF0FCA0AC"
    )
    port map (
      ADR5 => x_8_IBUF_0,
      ADR1 => x_cs(8),
      ADR3 => rdy_cs_PWR_4_o_AND_2_o,
      ADR0 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out8,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N114
    );
  Mmux_calc_sqr_root_approx_v1261_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y8",
      INIT => X"FFF3FFF3FFFF0000"
    )
    port map (
      ADR0 => '1',
      ADR1 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR4 => rdy_cs_PWR_4_o_AND_2_o_mmx_out10_0,
      ADR2 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out9,
      ADR5 => calc_sqr_root_calcPath_v,
      ADR3 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N117
    );
  Mmux_calc_sqr_root_approx_v1261_SW3 : X_LUT6
    generic map(
      LOC => "SLICE_X13Y8",
      INIT => X"EEEEFAFAEE44FA50"
    )
    port map (
      ADR1 => x_9_IBUF_0,
      ADR2 => x_cs(9),
      ADR4 => rdy_cs_PWR_4_o_AND_2_o,
      ADR5 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out9,
      ADR0 => calc_sqr_root_calcPath_v,
      ADR3 => corrNeg_cs_counter_cs_3_AND_41_o11,
      O => N119
    );
  Mmux_calc_sqr_root_approx_v1301_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X14Y5",
      INIT => X"FFFF00FFFFFF00FF"
    )
    port map (
      ADR0 => '1',
      ADR1 => '1',
      ADR2 => '1',
      ADR5 => '1',
      ADR4 => x_cs(11),
      ADR3 => counter_cs_3_PWR_4_o_LessThan_16_o,
      O => N218
    );
  Mmux_calc_sqr_root_approx_v1231_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X14Y8",
      INIT => X"FFF0CFC0FFF0FFF0"
    )
    port map (
      ADR0 => '1',
      ADR5 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR3 => rdy_cs_PWR_4_o_AND_2_o_mmx_out7,
      ADR1 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out6,
      ADR2 => calc_sqr_root_calcPath_v,
      ADR4 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N102
    );
  Mmux_calc_sqr_root_approx_v1211_SW1 : X_LUT6
    generic map(
      LOC => "SLICE_X14Y8",
      INIT => X"FCFCFCFCB8B8FCFC"
    )
    port map (
      ADR3 => '1',
      ADR4 => corrNeg_cs_PWR_4_o_AND_42_o,
      ADR2 => rdy_cs_PWR_4_o_AND_2_o_mmx_out5,
      ADR0 => counter_cs_3_PWR_4_o_LessThan_16_o_mmx_out4,
      ADR1 => calc_sqr_root_calcPath_v,
      ADR5 => corrNeg_cs_counter_cs_3_AND_41_o1_1823,
      O => N92
    );
  corrNeg_cs_counter_cs_3_AND_41_o1_1 : X_LUT6
    generic map(
      LOC => "SLICE_X14Y8",
      INIT => X"0001000100000000"
    )
    port map (
      ADR4 => '1',
      ADR0 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_1_Q,
      ADR3 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_3_Q,
      ADR2 => Madd_counter_cs_3_GND_4_o_add_41_OUT_cy_0_Q,
      ADR1 => Madd_counter_cs_3_GND_4_o_add_41_OUT_lut_2_Q,
      ADR5 => calc_sqr_root_corrNeg_v,
      O => corrNeg_cs_counter_cs_3_AND_41_o1_1823
    );
  NlwBufferBlock_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_0_Q : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => x_cs_11_GND_4_o_mux_26_OUT_0_Q,
      O => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_0_Q
    );
  NlwBufferBlock_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_1_Q : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => x_cs_11_GND_4_o_mux_26_OUT_1_Q,
      O => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_1_Q
    );
  NlwBufferBlock_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_2_Q : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => x_cs_11_GND_4_o_mux_26_OUT_2_Q,
      O => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_2_Q
    );
  NlwBufferBlock_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_3_Q : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => x_cs_11_GND_4_o_mux_26_OUT_3_Q,
      O => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_3_DI_3_Q
    );
  NlwBufferBlock_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_0_Q : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => x_cs_11_GND_4_o_mux_26_OUT_4_Q,
      O => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_0_Q
    );
  NlwBufferBlock_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_1_Q : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => x_cs_11_GND_4_o_mux_26_OUT_5_Q,
      O => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_1_Q
    );
  NlwBufferBlock_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_2_Q : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => x_cs_11_GND_4_o_mux_26_OUT_6_Q,
      O => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_2_Q
    );
  NlwBufferBlock_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_3_Q : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => x_cs_11_GND_4_o_mux_26_OUT_7_Q,
      O => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_cy_7_DI_3_Q
    );
  NlwBufferBlock_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_0_Q : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => x_cs_11_GND_4_o_mux_26_OUT_8_Q,
      O => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_0_Q
    );
  NlwBufferBlock_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_1_Q : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => x_cs_11_GND_4_o_mux_26_OUT_9_Q,
      O => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_1_Q
    );
  NlwBufferBlock_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_2_Q : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => x_cs_11_GND_4_o_mux_26_OUT_10_Q,
      O => NlwBufferSignal_Msub_GND_4_o_GND_4_o_sub_28_OUT_11_0_xor_11_DI_2_Q
    );
  NlwBufferBlock_fx_10_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => fx_cs(10),
      O => NlwBufferSignal_fx_10_OBUF_I
    );
  NlwBufferBlock_fx_0_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => fx_cs(0),
      O => NlwBufferSignal_fx_0_OBUF_I
    );
  NlwBufferBlock_fx_11_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => fx_cs(11),
      O => NlwBufferSignal_fx_11_OBUF_I
    );
  NlwBufferBlock_fx_1_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => fx_cs(1),
      O => NlwBufferSignal_fx_1_OBUF_I
    );
  NlwBufferBlock_fx_2_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => fx_cs(2),
      O => NlwBufferSignal_fx_2_OBUF_I
    );
  NlwBufferBlock_fx_3_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => fx_cs(3),
      O => NlwBufferSignal_fx_3_OBUF_I
    );
  NlwBufferBlock_fx_4_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => fx_cs(4),
      O => NlwBufferSignal_fx_4_OBUF_I
    );
  NlwBufferBlock_rdy_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => rdy_cs_1782,
      O => NlwBufferSignal_rdy_OBUF_I
    );
  NlwBufferBlock_fx_5_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => fx_cs(5),
      O => NlwBufferSignal_fx_5_OBUF_I
    );
  NlwBufferBlock_fx_6_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => fx_cs(6),
      O => NlwBufferSignal_fx_6_OBUF_I
    );
  NlwBufferBlock_fx_7_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => fx_cs(7),
      O => NlwBufferSignal_fx_7_OBUF_I
    );
  NlwBufferBlock_fx_8_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => fx_cs(8),
      O => NlwBufferSignal_fx_8_OBUF_I
    );
  NlwBufferBlock_fx_9_OBUF_I : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => fx_cs(9),
      O => NlwBufferSignal_fx_9_OBUF_I
    );
  NlwBufferBlock_clk_BUFGP_BUFG_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP_IBUFG_0,
      O => NlwBufferSignal_clk_BUFGP_BUFG_IN
    );
  NlwBufferBlock_fx_cs_3_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_fx_cs_3_CLK
    );
  NlwBufferBlock_fx_cs_3_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_approx_v(3),
      O => NlwBufferSignal_fx_cs_3_IN
    );
  NlwBufferBlock_fx_cs_2_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_fx_cs_2_CLK
    );
  NlwBufferBlock_fx_cs_2_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_approx_v(2),
      O => NlwBufferSignal_fx_cs_2_IN
    );
  NlwBufferBlock_fx_cs_1_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_fx_cs_1_CLK
    );
  NlwBufferBlock_fx_cs_1_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_approx_v(1),
      O => NlwBufferSignal_fx_cs_1_IN
    );
  NlwBufferBlock_fx_cs_0_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_fx_cs_0_CLK
    );
  NlwBufferBlock_fx_cs_0_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_approx_v(0),
      O => NlwBufferSignal_fx_cs_0_IN
    );
  NlwBufferBlock_approx_cs_2_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_approx_cs_2_CLK
    );
  NlwBufferBlock_approx_cs_1_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_approx_cs_1_CLK
    );
  NlwBufferBlock_approx_cs_0_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_approx_cs_0_CLK
    );
  NlwBufferBlock_fx_cs_11_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_fx_cs_11_CLK
    );
  NlwBufferBlock_fx_cs_10_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_fx_cs_10_CLK
    );
  NlwBufferBlock_fx_cs_9_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_fx_cs_9_CLK
    );
  NlwBufferBlock_fx_cs_9_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_approx_v(9),
      O => NlwBufferSignal_fx_cs_9_IN
    );
  NlwBufferBlock_fx_cs_8_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_fx_cs_8_CLK
    );
  NlwBufferBlock_fx_cs_8_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_approx_v(8),
      O => NlwBufferSignal_fx_cs_8_IN
    );
  NlwBufferBlock_fx_cs_7_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_fx_cs_7_CLK
    );
  NlwBufferBlock_fx_cs_7_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_approx_v(7),
      O => NlwBufferSignal_fx_cs_7_IN
    );
  NlwBufferBlock_fx_cs_6_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_fx_cs_6_CLK
    );
  NlwBufferBlock_fx_cs_6_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_approx_v(6),
      O => NlwBufferSignal_fx_cs_6_IN
    );
  NlwBufferBlock_fx_cs_5_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_fx_cs_5_CLK
    );
  NlwBufferBlock_fx_cs_4_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_fx_cs_4_CLK
    );
  NlwBufferBlock_fx_cs_4_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_approx_v(4),
      O => NlwBufferSignal_fx_cs_4_IN
    );
  NlwBufferBlock_approx_cs_6_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_approx_cs_6_CLK
    );
  NlwBufferBlock_approx_cs_6_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_approx_v(6),
      O => NlwBufferSignal_approx_cs_6_IN
    );
  NlwBufferBlock_approx_cs_5_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_approx_cs_5_CLK
    );
  NlwBufferBlock_approx_cs_5_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_approx_v(5),
      O => NlwBufferSignal_approx_cs_5_IN
    );
  NlwBufferBlock_approx_cs_4_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_approx_cs_4_CLK
    );
  NlwBufferBlock_approx_cs_3_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_approx_cs_3_CLK
    );
  NlwBufferBlock_approx_cs_8_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_approx_cs_8_CLK
    );
  NlwBufferBlock_approx_cs_7_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_approx_cs_7_CLK
    );
  NlwBufferBlock_approx_cs_11_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_approx_cs_11_CLK
    );
  NlwBufferBlock_approx_cs_11_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_approx_v(11),
      O => NlwBufferSignal_approx_cs_11_IN
    );
  NlwBufferBlock_approx_cs_10_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_approx_cs_10_CLK
    );
  NlwBufferBlock_approx_cs_10_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_approx_v(10),
      O => NlwBufferSignal_approx_cs_10_IN
    );
  NlwBufferBlock_approx_cs_9_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_approx_cs_9_CLK
    );
  NlwBufferBlock_counter_cs_3_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_counter_cs_3_CLK
    );
  NlwBufferBlock_counter_cs_3_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_counter_v(3),
      O => NlwBufferSignal_counter_cs_3_IN
    );
  NlwBufferBlock_counter_cs_2_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_counter_cs_2_CLK
    );
  NlwBufferBlock_counter_cs_1_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_counter_cs_1_CLK
    );
  NlwBufferBlock_counter_cs_0_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_counter_cs_0_CLK
    );
  NlwBufferBlock_counter_cs_0_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_counter_v(0),
      O => NlwBufferSignal_counter_cs_0_IN
    );
  NlwBufferBlock_x_cs_5_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_x_cs_5_CLK
    );
  NlwBufferBlock_x_cs_4_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_x_cs_4_CLK
    );
  NlwBufferBlock_counter_cs_2_2_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_counter_cs_2_2_CLK
    );
  NlwBufferBlock_counter_cs_2_2_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_counter_v(2),
      O => NlwBufferSignal_counter_cs_2_2_IN
    );
  NlwBufferBlock_counter_cs_2_1_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_counter_cs_2_1_CLK
    );
  NlwBufferBlock_counter_cs_2_1_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_counter_v(2),
      O => NlwBufferSignal_counter_cs_2_1_IN
    );
  NlwBufferBlock_rdy_cs_2_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_rdy_cs_2_CLK
    );
  NlwBufferBlock_rdy_cs_2_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_rdy_v,
      O => NlwBufferSignal_rdy_cs_2_IN
    );
  NlwBufferBlock_rdy_cs_1_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_rdy_cs_1_CLK
    );
  NlwBufferBlock_rdy_cs_1_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_rdy_v,
      O => NlwBufferSignal_rdy_cs_1_IN
    );
  NlwBufferBlock_x_cs_7_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_x_cs_7_CLK
    );
  NlwBufferBlock_x_cs_6_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_x_cs_6_CLK
    );
  NlwBufferBlock_corrNeg_cs_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_corrNeg_cs_CLK
    );
  NlwBufferBlock_rdy_cs_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_rdy_cs_CLK
    );
  NlwBufferBlock_counter_cs_0_2_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_counter_cs_0_2_CLK
    );
  NlwBufferBlock_counter_cs_0_1_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_counter_cs_0_1_CLK
    );
  NlwBufferBlock_counter_cs_0_1_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_counter_v(0),
      O => NlwBufferSignal_counter_cs_0_1_IN
    );
  NlwBufferBlock_x_cs_11_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_x_cs_11_CLK
    );
  NlwBufferBlock_x_cs_10_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_x_cs_10_CLK
    );
  NlwBufferBlock_x_cs_1_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_x_cs_1_CLK
    );
  NlwBufferBlock_x_cs_0_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_x_cs_0_CLK
    );
  NlwBufferBlock_counter_cs_1_2_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_counter_cs_1_2_CLK
    );
  NlwBufferBlock_counter_cs_1_2_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_counter_v(1),
      O => NlwBufferSignal_counter_cs_1_2_IN
    );
  NlwBufferBlock_counter_cs_1_1_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_counter_cs_1_1_CLK
    );
  NlwBufferBlock_counter_cs_1_1_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_counter_v(1),
      O => NlwBufferSignal_counter_cs_1_1_IN
    );
  NlwBufferBlock_counter_cs_3_2_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_counter_cs_3_2_CLK
    );
  NlwBufferBlock_counter_cs_3_1_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_counter_cs_3_1_CLK
    );
  NlwBufferBlock_counter_cs_3_1_IN : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => calc_sqr_root_counter_v(3),
      O => NlwBufferSignal_counter_cs_3_1_IN
    );
  NlwBufferBlock_x_cs_3_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_x_cs_3_CLK
    );
  NlwBufferBlock_x_cs_2_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_x_cs_2_CLK
    );
  NlwBufferBlock_x_cs_9_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_x_cs_9_CLK
    );
  NlwBufferBlock_x_cs_8_CLK : X_BUF
    generic map(
      PATHPULSE => 202 ps
    )
    port map (
      I => clk_BUFGP,
      O => NlwBufferSignal_x_cs_8_CLK
    );
  NlwBlock_dut_GND : X_ZERO
    port map (
      O => GND
    );
  NlwBlock_dut_VCC : X_ONE
    port map (
      O => VCC
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => GSR);
  NlwBlockTOC : X_TOC
    port map (O => GTS);

end Structure;

