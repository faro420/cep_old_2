--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- Testbench for dut
library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;

entity tbDut is
    generic (
        msbPos : natural := 11
    );
end tbDut;

architecture beh of tbDut is
    signal x_s         : std_logic_vector(msbPos downto 0);
    signal req_s       : std_logic;
    signal clk_s       : std_logic;
    signal nRes_s      : std_logic;
    signal fx_s        : std_logic_vector(msbPos downto 0);
    signal rdy_s       : std_logic;
    
    component sg is
        port (
            x         : out std_logic_vector(msbPos downto 0);
            req       : out std_logic;
            clk       : out std_logic;
            nRes      : out std_logic
        );
    end component sg;
    for all : sg use entity work.sg( beh );

    component dut is
        port(
            x    : in std_logic_vector(msbPos downto 0);
            req  : in std_logic;
            clk  : in std_logic;
            nRes : in std_logic;
            
            fx  : out std_logic_vector(msbPos downto 0);
            rdy : out std_logic
        );
    end component;
    for all: dut use entity work.dut( Structure );

begin
    sg_i : sg
        port map (
            x         => x_s,
            req       => req_s,
            clk       => clk_s,
            nRes      => nRes_s
        );
    
    dut_i : dut
        port map (
            x    => x_s,
            req  => req_s,
            clk  => clk_s,
            nRes => nRes_s,
            fx   => fx_s,
            rdy  => rdy_s
        );
end beh;